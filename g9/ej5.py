import numpy as np
import math
from scipy.stats import t

A = [0.8, 0.9, 1.05, 1.2, 1.3, 1.3]
B = [1.0, 1.4, 1.7, 1.9, 2.3]

n = len(A)
m = len(B)

alphas = [0.1, 0.05, 0.025, 0.01]

print('alpha\tUc')
for alpha in alphas:
  Uc = t.ppf(1-alpha,df = n+m-2)
  print(str(alpha)+'\t'+str(Uc))


meanA = np.mean(A)
meanB = np.mean(B)
sx2 = np.sum((A-meanA)*(A-meanA))
sy2 = np.sum((B-meanB)*(B-meanB))
Um = -(meanA-meanB)*np.sqrt((m+n-2)/((1/m+1/n)*(sx2+sy2)))
print('Um = '+str(Um))
print('p val = '+str(1-t.cdf(Um,df = n+m-2)))
