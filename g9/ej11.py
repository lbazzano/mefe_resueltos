from scipy.stats import wilcoxon
from scipy.stats import ranksums
from scipy.stats import kstest
from scipy.special import comb
import numpy as np
import math
from scipy.stats import t
from scipy.stats import norm


A = [81, 82, 87, 93, 102, 104, 108, 112, 116, 122, 125, 131, 131, 133, 134, 139, 139, 142, 144, 146, 152, 156, 182, 202, 206, 216, 226, 270]
B = [50, 64, 68, 76, 79, 83, 88, 96, 97, 98, 99, 103, 105, 107, 113, 114, 115, 126, 128, 130, 132, 138, 150, 169, 171]

#print(1+2+3+4+5+8+10+12+13+14+15+17+19+20+23+24+25+29+30+31+34+37+43+46+47)
#print(6+7+9+11+16+18+21+22+26+27+28+32+33+35+36+38+39+40+41+42+44+45+48+49+50+51+52+53)

n = len(A)
m = len(B)


# rachas
kmin = int(12/2)
kmax = int(n/2)
pval=0
for k in range(kmin,kmax):
  pval += 2*comb(n-1,k-1)*comb(m-1,k-1) / comb(n+m,n)
print('\n\nRachas:\npval:'+str(pval))



#Es A->n / B->m
# wilcoxon
Wmin  = n*(n+1)/2
Wmax  = n*(n+1)/2 + n*m
Wmean = (n/2)*(n+m+1)
Wvar  = (n*m/12)*(n+m+1)
Wobs1  = 1+2+3+4+5+8+10+12+13+14+15+17+19+20+23+24+25+29+30+31+34+37+43+46+47
Wobs2  = 6+7+9+11+16+18+21+22+26+27+28+32+33+35+36+38+39+40+41+42+44+45+48+49+50+51+52+53
z1 = (Wobs1 - Wmean -0*  0.5) / math.sqrt(Wvar)
z2 = (Wobs2 - Wmean -0*  0.5) / math.sqrt(Wvar)
#print('Wmin,Wmean,Wmax,sqrt(Wvar),Wobs1,Wobs2')
#print(Wmin,Wmean,Wmax,math.sqrt(Wvar),Wobs1,Wobs2)
#print('z1,norm.cdf(z1)')
#print(z1,norm.cdf(z1))
#print('z2,1-norm.cdf(z2)')
#print(z2,1-norm.cdf(z2))
print('\n\n Wilcoxon:')
print(norm.cdf(z1))
print(1-norm.cdf(z2))



# kolmogorov
print('\n\nKolmogorov:\n'+str(kstest(A,B)))



# rank
#print(\n\nranksums(A,B))

