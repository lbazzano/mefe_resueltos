import numpy as np
import math

vals = np.array([80.482, 80.423, 80.38, 80.61, 80.41])
errs = np.array([0.091,  0.112,  0.12,  0.15,  0.18 ])

print('promedio: '+str(np.mean(vals)))
mean = np.average(vals, weights=1/errs)
print('promedio pesado: '+str(mean))

s = 0
for val,err in zip(vals,errs):
   s += math.pow( (val - mean) / err , 2)

print('S = sumatoria ( yi - f(xi) / sigma )^2')
print('n (=5) es la cantidad de datos y k (=1) es la cantidad de parámetros que estimé en f(xi) = valor medio(xi)')
print('S sigue una distribución Chi2 de n-k (=4) gl')
print('S medido:'+str(s))

from scipy.stats import chi2
df = 4
p = 1 - chi2.cdf(s,df)
print('P de S medido o mayor:'+str(p))

