import numpy as np
import math
from scipy.stats import f

A = [0.8, 0.9, 1.05, 1.2, 1.3, 1.3]
B = [1.0, 1.4, 1.7, 1.9, 2.3]

n = len(A)
m = len(B)

meanA = np.mean(A)
meanB = np.mean(B)
sA2 = np.sum((A-meanA)*(A-meanA))/(n-1)
sB2 = np.sum((B-meanB)*(B-meanB))/(m-1)

#print('chequeo que esto de 5.47: '+str(sB2/sA2))

# F medido (asumiendo que sigmaA y sigmaB son iguales)
Fm = (sB2/sA2)
dfm = m-1
dfn = n-1
pval = 1 - f.cdf(Fm,dfm,dfn)
print('p valor: '+str(pval))
