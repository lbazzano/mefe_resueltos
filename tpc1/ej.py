from scipy.stats import binom
from scipy.stats import poisson
from random import random
import matplotlib.pyplot as plt
import numpy as np

# Esta función la voy a usar en todos los incisos:
def bernoulli(n,p):       # Tiene como input la cantidad de experimentos de bernoulli n y la probabilidad de éxito p.
  si = 0                  # cantidad de experimentos exitosos
  for foton in range(n):  # Para cada experimento de bernoulli (lo llamo fotón por el inciso b)
    rand = random()       # Genero un numero aleatorio del 0 al 1
    if rand <= p:         # y lo comparo con la probabilidad de éxito.
       si += 1            # si es menor, sumo 1 experimento exitoso
  return int(si)          # devuelvo la cantidad de experimentos exitosos

#Esta función la voy a usar en el inciso 6.
def histo_norm_err(histo,n): # Armo esto para calcular los errores cuando normalizo más fácil
  heights, bins = np.histogram(histo,bins=n+1,range=[0,n+1])
  err = np.sqrt(heights)/np.sum(heights)
  bins_corr = bins[:-1]
  heights_norm = heights/np.sum(heights)
  return bins_corr, heights_norm, err



print('Inciso 1: Experimentos de bernoulli')########################################################################
# Fijo p que es la probabilidad del primer inciso y n la cantidad de experimentos de Bernoullli:
n = 15             # Escribo n a mano
p = 0.75           # Escribo p a mano
# Printeo el resultado:
print('Probabilidad de éxito: '+str(p))
print('Cantidad de experimentos: '+str(n))
print('Cantidad de éxitos: '+str(bernoulli(n,p)))###################################################################



print('\nInciso 2: Detector de fotones')############################################################################
N = 1000         # Cantidad de veces que repito el experimento con n = 15
n = 15           # Son 15 fotones por exerimento
eff = 0.75       # La eficiencia del detector es la proba de detección
# Genero mi histograma de experimentos en función de la cantidad de éxitos
# Este va a ser el vector de cantidad de éxitos en cada experimento
histo = [bernoulli(n,eff) for exp in range(N)]
# Para N experimentos me fijo cuantos de los n fotones fueron detectados en el detector con eficiencia eff
# Quiero comparar a mi histograma con la curva teórica, que es una binomial (dos resultados posibles: detecta o no):
K = np.linspace(0, n, n+1)                  # Esto es el eje x de la curva teórica
binomial_ = [binom.pmf(k,n,eff) for k in K] # probabilidad de una dada cantidad de éxitos en un experimento
# Ahora grafico las dos cosas, con los resultados normalizados:
plt.hist(histo,density=True,label='Resultados',bins=n+1,range=[-0.5,n+0.5],align='mid',rwidth=0.9)
plt.plot(K,binomial_,label='Binomial',marker='.',color='red',alpha=0.5)
plt.xlabel('Fotones detectados')
plt.ylabel('Probabilidad')
plt.legend()
plt.grid()
plt.title('Inciso 2')
plt.show()  
print('Binomial(n = '+str(n)+', p = '+str(p)+')')###################################################################



print('\nInciso 3: Fuente de fotones')##############################################################################
I = 15          # Intensidad en fotones / segundos
Delta_t = 1     # Delta t en segundos
m = 1000        # Cantidad de intervalos en Delta_t
dt = Delta_t/m  # Diferencial tiempo
p = I*dt        # Aproximación de la probabilidad de que la fuente emita un fotón
N = 1000        # Cantidad de veces que voy a realizar el experimento
# Tomando un dt lo suficientemente chiquito se puede asumir que la probabilidad de tener más de 1 es bajísima.
# Entonces siempre voy a tener 0 o 1. La probabilidad de 1 ya es baja, con los números del enunciado es ~1.5%.
# print('Probabilidad de 1 fotón en dt = I*dt = '+str(I)+'*'+str(dt)+' = '+str(p)) # Si quiero ver la proba
# Este va a ser el vector de cantidad de éxitos en cada experimento que uso para hacer el histograma
histo = [bernoulli(m,p) for dt in range(N)] # Para una cantidad m de dt's, me fijo cuantos fotones se emiten.
# Repito N veces.
# Quiero comparar a mi histograma con la curva teórica, que es una poisson:
K = np.linspace(0, m, m+1)                   # Esto es el eje x de la curva teórica
mu = I                                       # Parámetro de Poisson
poisson_ = [poisson.pmf(k,mu) for k in K]    # probabilidad de una dada cantidad de éxitos
# Ahora grafico los resultados normalizados y comparo con poisson:
bins, heights, c = plt.hist(histo,density=True,label='Resultados',bins=m+1,range=[-0.5,m+0.5],align='mid',rwidth=0.9)
plt.plot(K,poisson_,label='Poisson',marker='.',color='red',alpha=0.5)
plt.xlabel('Fotones emitidos')
plt.ylabel('Probabilidad')
plt.legend()
plt.xlim(0,np.max(histo)+mu)
plt.grid()
plt.title('Inciso 3')
plt.show()
print('Poisson(mu = '+ str(mu)+')')#################################################################################



print('\nInciso 4: Fotones emitidos y detectados')##################################################################
# Primero, genero mi histograma de fotones emitidos (las variables son las mismas de antes)
histo_emitidos = [bernoulli(m,p) for dt in range(N)] # Para una cantidad m de dt's, me fijo cuántos fotones emite.
# Repito N veces.
# Segundo, me fijo cuántos de esos son detectados en cada experimento de Delta_t segundos:
# Genero mi histograma de cantidad de fotones detectados
histo_detectados = [bernoulli(n,eff) for n in histo_emitidos]
# Para N experimentos me fijo cuantos de los n fotones fueron detectados en el detector con eficiencia eff
# Quiero comparar a mi histograma con la curva teórica, que es una poisson (composición de poisson con binomial):
K = np.linspace(0, m, m+1)                   # Esto es el eje x de la curva teórica
mu = I*eff                                   # Parámetro de Poisson (Sale de hacer la composición)
poisson_ = [poisson.pmf(k,mu) for k in K]   # probabilidad de una dada cantidad de éxitos
# Ahora grafico los resultados normalizados y comparo con poisson con mu = I*eff*t:
bins, heights, c = plt.hist(histo_detectados,density=True,label='Resultados',
                            bins=m+1,range=[-0.5,m+0.5],align='mid',rwidth=0.9)
plt.plot(K,poisson_,label='Poisson',marker='.',color='red',alpha=0.5)
plt.xlabel('Fotones emitidos y detectados')
plt.ylabel('Probabilidad')
plt.legend()
plt.xlim(0,np.max(histo_detectados)+mu)
plt.grid()
plt.title('Inciso 4')
plt.show()
print('Poisson(mu = '+ str(mu)+')')#################################################################################



print('\nInciso 5: Probabilidad Efectiva')##########################################################################
# Me fijo cuántos fotones son emitidos y detectados usando una eficiencia/proba efectiva:
# Genero mi histograma de detectados usando efficiencia de detección * proba de emisión como la proba efectiva
histo_detectados = [bernoulli(m,eff*p) for dt in range(N)]
# Para N experimentos me fijo cuantos de los n fotones fueron detectados en el detector
# Quiero comparar a mi histograma con la curva teórica, que es una poisson (ya definida en el inciso anterior)
# Ahora grafico los resultados normalizados y comparo con poisson con mu = I*eff*t:
bins, heights, c = plt.hist(histo_detectados,density=True,label='Resultados',
                            bins=m+1,range=[-0.5,m+0.5],align='mid',rwidth=0.9)
plt.plot(K,poisson_,label='Poisson',marker='.',color='red',alpha=0.5)
plt.xlabel('Fotones emitidos y detectados')
plt.ylabel('Probabilidad')
plt.legend()
plt.xlim(0,np.max(histo_detectados)+mu)
plt.grid()
plt.title('Inciso 5')
plt.show()
print('Poisson(mu = '+ str(mu)+')')###############################################################################



print('\nInciso 6: Incertezas')#####################################################################################
# Este inciso es básicamente plotear todo con errores (calculados como la raiz de la altura de cada bin).
fig = plt.figure()
fig.suptitle('Inciso 6', fontsize=16)
# Inciso 2
ax2 = fig.add_subplot(221)
N = 1000
n = 15
eff = 0.75
histo = [bernoulli(n,eff) for exp in range(N)]
bins_corr, heights_norm, err = histo_norm_err(histo,n)
ax2.bar(bins_corr, heights_norm, width=0.9,yerr=err,label='Resultados')
K = np.linspace(0, n, n+1)
binomial_ = [binom.pmf(k,n,eff) for k in K]
ax2.plot(K,binomial_,label='Binomial',marker='.',color='red',alpha=0.5)
ax2.set(xlabel='Fotones Detectados', ylabel='Probabilidad')
ax2.legend()
ax2.set_title('Inciso 2')
ax2.grid()
# Inciso 3
ax3 = fig.add_subplot(222)
histo = [bernoulli(m,p) for dt in range(N)]
bins_corr, heights_norm, err = histo_norm_err(histo,m)
ax3.bar(bins_corr, heights_norm, width=0.9,yerr=err,label='Resultados')
K = np.linspace(0, m, m+1)                 
mu = I                                   
poisson_ = [poisson.pmf(k,mu) for k in K]
ax3.plot(K,poisson_,label='Poisson',marker='.',color='red',alpha=0.5)
ax3.set(xlabel='Fotones emitidos', ylabel='Probabilidad')
ax3.set_xlim(0,np.max(histo)+mu)
ax3.legend()
ax3.set_title('Inciso 3')
ax3.grid()
# Inciso 4
ax4 = fig.add_subplot(223)
histo_emitidos = [bernoulli(m,p) for dt in range(N)]
eff = 0.75
histo_detectados = [bernoulli(n,eff) for n in histo_emitidos]
bins_corr, heights_norm, err = histo_norm_err(histo_detectados,m)
ax4.bar(bins_corr, heights_norm, width=0.9,yerr=err,label='Resultados')
K = np.linspace(0, m, m+1)                 
mu = I*eff                               
poisson_ = [poisson.pmf(k,mu) for k in K]
ax4.plot(K,poisson_,label='Poisson',marker='.',color='red',alpha=0.5)
ax4.set(xlabel='Fotones detectados', ylabel='Probabilidad')
ax4.set_xlim(0,np.max(histo_detectados)+mu)
ax4.legend()
ax4.set_title('Inciso 4')
ax4.grid()
# Inciso 5
ax5 = fig.add_subplot(224)
histo_detectados = [bernoulli(m,eff*p) for dt in range(N)]
bins_corr, heights_norm, err = histo_norm_err(histo_detectados,m)
ax5.bar(bins_corr, heights_norm, width=0.9,yerr=err,label='Resultados')
ax5.plot(K,poisson_,label='Poisson',marker='.',color='red',alpha=0.5)
ax5.set(xlabel='Fotones detectados', ylabel='Probabilidad')
ax5.set_xlim(0,np.max(histo_detectados)+mu)
ax5.legend()
ax5.set_title('Inciso 5')
ax5.grid()
plt.show()##########################################################################################################
