from scipy.stats import binom
from scipy.stats import norm
from scipy.stats import poisson
import numpy as np
import matplotlib.pyplot as plt

print('\n ej 1 a')

#B1
n = 5
p = 0.2
k_values = list(range(n + 1))
mean, var = binom.stats(n, p)
print('B1(5,0.2) / esperanza = '+str(mean)+' / varianza = '+str(var))
dist = [binom.pmf(k, n, p) for k in k_values ]
plt.plot(k_values,dist,'r*:')

loc = n*p
scale = np.sqrt(loc*(1-p))
mean, var = norm.stats(loc,scale)
print('N1('+str(loc)+','+str(scale)+') / esperanza = '+str(mean)+' / varianza = '+str(var))
plt.plot(k_values,norm.pdf(k_values,loc,scale),'ro-')

#B2
n = 30
p = 0.4
k_values = list(range(n + 1))
mean, var = binom.stats(n, p)
print('B2(30,0.4) / esperanza = '+str(mean)+' / varianza = '+str(var))
dist = [binom.pmf(k, n, p) for k in k_values ]
plt.plot(k_values,dist,'b*:')

loc = n*p
scale = np.sqrt(loc*(1-p))
mean, var = norm.stats(loc, scale)
print('N2('+str(loc)+','+str(scale)+') / esperanza = '+str(mean)+' / varianza = '+str(var))
plt.plot(k_values,norm.pdf(k_values,loc,scale),'bo-')

plt.show()



print('\n ej 1 b')
n=60

#P1
lamb = 4
k_values = list(range(n + 1))
mean, var = poisson.stats(lamb)
print('P1(4) / esperanza = '+str(mean)+' / varianza = '+str(var))
dist = [poisson.pmf(k, lamb) for k in k_values ]
plt.plot(k_values,dist,'r*:')

loc = lamb
scale = np.sqrt(lamb)
mean, var = norm.stats(loc,scale)
print('N1('+str(loc)+','+str(scale)+') / esperanza = '+str(mean)+' / varianza = '+str(var))
plt.plot(k_values,norm.pdf(k_values,loc,scale),'ro-')

#P2
lamb = 10
k_values = list(range(n + 1))
mean, var = poisson.stats(lamb)
print('P2(10) / esperanza = '+str(mean)+' / varianza = '+str(var))
dist = [poisson.pmf(k, lamb) for k in k_values ]
plt.plot(k_values,dist,'b*:')

loc = lamb
scale = np.sqrt(lamb)
mean, var = norm.stats(loc, scale)
print('N2('+str(loc)+','+str(scale)+') / esperanza = '+str(mean)+' / varianza = '+str(var))
plt.plot(k_values,norm.pdf(k_values,loc,scale),'bo-')

#P3
lamb = 40
k_values = list(range(n + 1))
mean, var = poisson.stats(lamb)
print('P3(40) / esperanza = '+str(mean)+' / varianza = '+str(var))
dist = [poisson.pmf(k, lamb) for k in k_values ]
plt.plot(k_values,dist,'g*:')

loc = lamb
scale = np.sqrt(lamb)
mean, var = norm.stats(loc, scale)
print('N2('+str(loc)+','+str(scale)+') / esperanza = '+str(mean)+' / varianza = '+str(var))
plt.plot(k_values,norm.pdf(k_values,loc,scale),'go-')

plt.show()
