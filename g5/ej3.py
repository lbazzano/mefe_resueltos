from random import random
from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt

Y = np.zeros(10000)
for t in range(10000):
    Z = 0
    for j in range(50):
        x_j = random()
        Z += x_j
    y = (Z - 25) / np.sqrt(50/12)
    Y[t]=y

lim = 4
nbins=100
heights,bins,c = plt.hist(Y,bins = np.linspace(-lim,lim,nbins),density=True)

gauss = []
print(bins)
for b in bins[1:]-(bins[1]-bins[0])/2:
    gauss.append(norm.pdf(b))

plt.plot(bins[1:]-(bins[1]-bins[0])/2,gauss)

plt.show()
