from scipy.stats import binom
from scipy.stats import norm
import numpy as np

print('\n ej 2')
p = float(1.0/3.0)
n = 100
a = 40
b = 100

P = 0
for k in range (a,b+1):
  P += binom.pmf(k,n,p)
print('Suma exacta: '+str(P))

B = (b - n*p + 0.5) / np.sqrt(n*p*(1-p))
A = (a - n*p - 0.5) / np.sqrt(n*p*(1-p))
print('Fórmula aproximada: '+str(norm.cdf(B)-norm.cdf(A)))
