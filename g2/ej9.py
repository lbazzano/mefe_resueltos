from scipy.stats import binom
from scipy.stats import hypergeom


print('\n ej 9 a')
p = 0.2
n = 10
P = 0
for k in range(0,3):
    P += binom.pmf(k, n, p)
print(P)
print(1-P)



print('\n ej 9 b')
p = 0.2
n = 6
k = 1
P = binom.pmf(k, n, p)
print(P)


print('\n ej 9 c')
p = 0.2
n = 10
P_A = 0
for k in range(0,11):
    if k >= 5:
        P_Ak = 0
    elif k <= 4 and k >=2:
        N_ = 10
        K_ = n - k
        n_ = 6
        k_ = 6
        P_Ak = hypergeom.pmf(k_,N_,K_,n_)
        if k == 2:
            k_ = 5
            P_Ak += hypergeom.pmf(k_,N_,K_,n_)
    elif k <= 1:
        P_Ak = 1

    P_k = binom.pmf(k, n, p)
    P_A += P_Ak * P_k

P_ATres = hypergeom.pmf(6,10,7,6)
P_Tres = binom.pmf(3, 10, 0.2)
P = P_ATres * P_Tres / P_A
print(P)
