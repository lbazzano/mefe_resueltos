from scipy.stats import binom
from scipy.stats import hypergeom

N = 10
n = 5
K = 5

P = 0
for k in range(3,6):
  print(k)
  print(hypergeom.pmf(k,N,K,n))
  P += hypergeom.pmf(k,N,K,n)

print(P)
