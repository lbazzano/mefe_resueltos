from scipy.stats import binom

p=0.1666
k=10

for n in range(11,100):
    if binom.pmf(k, n, p) > binom.pmf(9, n, p) and binom.pmf(k, n, p) > binom.pmf(11, n, p):
        print(n)
