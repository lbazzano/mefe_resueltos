import numpy as np
import math
import matplotlib.pyplot as plt
import ROOT

print("Binomial:")
print("Cuanto es n?")
n = int(input())
print("Cuanto es p?")
p = float(input())
x=[]
y=[]
proba = 0
for k in range(1,n):
  y.append(ROOT.Math.binomial_pdf(int(k),float(p),int(n)))
  x.append(k)
  if k >= 3 and k<=10:
    proba += ROOT.Math.binomial_pdf(int(k),float(p),int(n))
    print(ROOT.Math.binomial_pdf(int(k),float(p),int(n)))

print('suma:')
print(proba)

plt.plot(x,y)
plt.title("Binomial")
plt.xlabel('k')
plt.ylabel('Probabilidad')
plt.grid(linestyle='-', linewidth=0.5)
plt.show()

print("Binomial Negativa:")
print("Cuanto es k?")
k = int(input())
x = []
y = []
N = n
proba = 0
for n in range(k,N):
  n_ = n - k
  k_ = k
  y.append(ROOT.Math.negative_binomial_pdf(int(n_),float(p),float(k_)))
  x.append(n)
  if n >= 1 and n<=6:
    print(n,k,p)
    proba += ROOT.Math.negative_binomial_pdf(int(n_),float(p),float(k))
    print(ROOT.Math.negative_binomial_pdf(int(n_),float(p),float(k)))
print('suma:')
print(proba)

plt.plot(x,y)
plt.title("Binomial negativa")
plt.xlabel('n')
plt.ylabel('Probabilidad')
plt.grid(linestyle='-', linewidth=0.5)
plt.show()


P = ROOT.Math.binomial_pdf(int(k),float(p),int(n))
print('P Binomial = '+str(P))
P = ROOT.Math.negative_binomial_pdf(int(k),float(p),float(n))
print('P Binomial negativa = '+str(P))
