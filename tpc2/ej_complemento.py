from scipy.integrate import quad
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad
import time


# Función que voy a usar para normalizar histogramas. Difrencio uniformes para que los bines queden lindos, todos equiprobables.
def histo_norm_err(histo,binwidth,isUniform=False,begin=0,end=1): # Armo esto para calcular los errores normalizados
  if isUniform:
    heights, bins = np.histogram(histo,bins=np.arange(begin, end+binwidth, binwidth))
  else:
    heights, bins = np.histogram(histo,bins=np.arange(min(histo), max(histo) + binwidth, binwidth))
  norm = np.sum(heights) * binwidth
  bins_corr = bins[:-1]+binwidth/2
  heights_norm = heights/norm
  #err = np.sqrt(heights)/norm # error poisson
  err = np.sqrt(heights)/norm # error binomial
  return bins_corr, heights_norm, err
# Curvas teóricas
def cauchy(t,a,b):
    return (1 / np.pi)*(a / (a * a+ (t-b)*(t-b)))
def exponential(t,lamb):
    return lamb * np.exp(-t*lamb)



Times_total = 20 # promedio estas veces
firstN = 1 # 10^1
lastN = 5  # 10^5
number = 20 # cantidad de puntos

N_all = np.logspace(firstN,lastN,num=number,base=10)
print(N_all)

T_total_CV = []
T_total_MC = []

for times in range(Times_total):
  T_CV = []
  T_MC = []
  print('\nTime = '+str(times))
  for N in N_all:
    N = int(N)
    print('N = '+str(N))
    a = 1                           # Parámetro de Cauchy (posición en x del centro de la barra en el G3P5)
    b = 0                           # Parámetro de Cauchy (posición en y del centro de la barra en el G3P5)
    start = time.time() # mido tiempo 
    X = np.random.uniform( 0, 1, N) # Uniforme X entre 0 y 1
    T = (X-0.5) * np.pi             # Uniforme Tita entre -pi/2 y pi/2
    Y = a * np.tan(T) + b           # Cauchy Y entre -inf e inf
    end = time.time()
    print('tiempo 1(a): '+str(end-start))
    T_CV.append(end-start)
  
    # cauchy:
    A = 1                           # Parámetro de Cauchy (posición en x del centro de la barra en el G3P5)
    B = 0                           # Parámetro de Cauchy (posición en y del centro de la barra en el G3P5)
    # parámetros para los límites del método:
    lim = 10
    a = -lim
    b = lim
    CauchyMax = cauchy(B,A,B)
    x = []
    n = len(x)
    start = time.time() # mido tiempo 
    while n < N: # Método de Monte Carlo (se repiten I, II y III hasta que n, cantidad de datos aceptados, sea lo que quiero)
      # I (Se generan dos números al azar y y z con distribución uniforme entre 0 y 1)
      y = np.random.uniform( 0, 1)# y Uniforme entre 0 y 1
      z = np.random.uniform( 0, 1)# z Uniforme entre 0 y 1
      # II (A partir de y y z se determinan u y v)
      u = a + (b - a) * y
      v = CauchyMax * z
      # III (si v es menor que f(u) incluyo x=u en la muestra)
      if v <= cauchy(u,A,B):
        x.append(u)
      n = len(x)
    end = time.time()
    print('tiempo 1(b) MC: '+str(end-start))
    T_MC.append(end-start)
  T_total_CV.append(T_CV)
  T_total_MC.append(T_MC)

Cv = []
CvErr = []
Mc = []
McErr = []
Div = []
Err = []
for l in range(len(N_all)):
  vec = []
  mc = []
  cv = []
  for k in range(len(T_total_MC)):
    vec.append(T_total_MC[k][l]/T_total_CV[k][l])
    mc.append(T_total_MC[k][l])
    cv.append(T_total_CV[k][l])
  Div.append(np.mean(vec))
  Err.append(np.std(vec))
  Mc.append(np.mean(mc))
  McErr.append(np.std(mc))
  Cv.append(np.mean(cv))
  CvErr.append(np.std(cv))

fig = plt.figure()
fig.suptitle('CV vs MC', fontsize=16)

ax1 = fig.add_subplot(211)
ax1.errorbar(N_all,Cv,yerr=CvErr,marker='o',color='crimson',label='Cambio de Variable')
ax1.errorbar(N_all,Mc,yerr=McErr,marker='o',color='darkorange',label='Monte Carlo')
ax1.set_xlabel('N')
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.legend()
ax1.set_ylabel('Tiempo de cálculo (s)')
ax1.grid()

ax1 = fig.add_subplot(212)
ax1.errorbar(N_all,Div,yerr=Err,marker='o',color='forestgreen')
ax1.set_xlabel('N')
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_ylabel('T(MC)/T(CV)')
ax1.grid()

'''
fig = plt.figure()
fig.suptitle('CV vs MC', fontsize=16)

ax1 = fig.add_subplot(311)
ax1.errorbar(N_all,Cv,yerr=CvErr,marker='o',color='crimson')
ax1.set_xlabel('N')
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_ylabel('Tiempo de cálculo CV (s)')
ax1.grid()

ax2 = fig.add_subplot(312)
ax2.errorbar(N_all,Mc,yerr=McErr,marker='o',color='darkorange')
ax2.set_xlabel('N')
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_ylabel('Tiempo de cálculo MC (s)')
ax2.grid()

ax1 = fig.add_subplot(313)
ax1.errorbar(N_all,Div,yerr=Err,marker='o',color='forestgreen')
ax1.set_xlabel('N')
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_ylabel('T(MC)/T(CV)')
ax1.grid()
'''
plt.show()

