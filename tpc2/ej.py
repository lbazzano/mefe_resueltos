from scipy.integrate import quad
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad
import time


# Función que voy a usar para normalizar histogramas. Difrencio uniformes para que los bines queden lindos, todos equiprobables.
def histo_norm_err(histo,binwidth,isUniform=False,begin=0,end=1): # Armo esto para calcular los errores normalizados
  if isUniform:
    heights, bins = np.histogram(histo,bins=np.arange(begin, end+binwidth, binwidth))
  else:
    heights, bins = np.histogram(histo,bins=np.arange(min(histo), max(histo) + binwidth, binwidth))
  norm = np.sum(heights) * binwidth
  bins_corr = bins[:-1]+binwidth/2
  heights_norm = heights/norm
  #err = np.sqrt(heights)/norm # error poisson
  err = np.sqrt(heights)/norm # error binomial
  return bins_corr, heights_norm, err
# Curvas teóricas
def cauchy(t,a,b):
    return (1 / np.pi)*(a / (a * a+ (t-b)*(t-b)))
def exponential(t,lamb):
    return lamb * np.exp(-t*lamb)



print('Inciso 1 (a): Método de Cambio de Variables')########################################################################################
N = 10000                       # Cantidad de números al azar (uniforme) a generar
a = 1                           # Parámetro de Cauchy (posición en x del centro de la barra en el G3P5)
b = 0                           # Parámetro de Cauchy (posición en y del centro de la barra en el G3P5)
start = time.time() # mido tiempo 
X = np.random.uniform( 0, 1, N) # Uniforme X entre 0 y 1
T = (X-0.5) * np.pi             # Uniforme Tita entre -pi/2 y pi/2
Y = a * np.tan(T) + b           # Cauchy Y entre -inf e inf
end = time.time()
print('tiempo 1(a): '+str(end-start))
e = np.pi/2
lim = 5*e
fig = plt.figure()
fig.suptitle('Inciso 1 (a)', fontsize=16)
# uniforme entre 0,1
ax1 = fig.add_subplot(311)
binwidth = 0.1
bins_corr, heights_norm, err = histo_norm_err(X,binwidth,isUniform=True)
ax1.bar(bins_corr, heights_norm, width=binwidth, yerr=err,label='X: Uniforme(0,1)',alpha=1,color='crimson')
ax1.set(xlabel='x', ylabel='Frecuencia')
ax1.set_xlim(-lim,lim)
ax1.legend()
ax1.grid()
# uniforme en tita
ax2 = fig.add_subplot(312)
binwidth = e/10
bins_corr, heights_norm, err = histo_norm_err(T,binwidth,isUniform=True,begin=-e,end=e)
ax2.bar(bins_corr, heights_norm, width=binwidth, yerr=err,label=r'$\theta$: Uniforme($-\frac{\pi}{2}$,$\frac{\pi}{2}$)',alpha=1,color='darkorange')
ax2.set(xlabel=r'$\theta$', ylabel='Frecuencia')
ax2.set_xlim(-lim,lim)
ax2.legend()
ax2.grid()
# cauchy
ax3 = fig.add_subplot(313)
binwidth = 0.2
bins_corr, heights_norm, err = histo_norm_err(Y,binwidth)
index = np.where(np.abs(bins_corr) < lim)
ax3.bar(bins_corr[index], heights_norm[index], width=0.2, yerr=err[index],label=r'Y: Cauchy($-\infty$,$\infty$)',alpha=1,color='forestgreen')
ax3.set(xlabel='y', ylabel='Frecuencia')
# cauchy teo
K = bins_corr[index]
Cauchy = [cauchy(k,a,b) for k in K]                                 # probabilidad
ax3.plot(K,Cauchy,label='Cauchy teórica',marker='.',color='red',alpha=0.5)
ax3.set_xlim(-lim,lim)
ax3.legend()
ax3.grid()
plt.show()



print('Inciso 1 (b): Método de Monte Carlo')#############################################################################################
N = 10000                       # Cantidad de números (x) al azar que quiero generar
# cauchy:
A = 1                           # Parámetro de Cauchy (posición en x del centro de la barra en el G3P5)
B = 0                           # Parámetro de Cauchy (posición en y del centro de la barra en el G3P5)
# parámetros para los límites del método:
lim = 10
a = -lim
b = lim
CauchyMax = cauchy(B,A,B)
x = []
n = len(x)
start = time.time() # mido tiempo 
while n < N: # Método de Monte Carlo (se repiten I, II y III hasta que n, cantidad de datos aceptados, sea lo que quiero)
  # I (Se generan dos números al azar y y z con distribución uniforme entre 0 y 1)
  y = np.random.uniform( 0, 1)# y Uniforme entre 0 y 1
  z = np.random.uniform( 0, 1)# z Uniforme entre 0 y 1
  # II (A partir de y y z se determinan u y v)
  u = a + (b - a) * y
  v = CauchyMax * z
  # III (si v es menor que f(u) incluyo x=u en la muestra)
  if v <= cauchy(u,A,B):
    x.append(u)
  n = len(x)
end = time.time()
print('tiempo 1(b) MC: '+str(end-start))
# Cauchy
binwidth = 0.2
bins_corr, heights_norm, err = histo_norm_err(x,binwidth)
K = quad(cauchy,a,b, args=(A,B))[0]                                                                   # Tomo la integral de Cauchy entre los límites que fijé 
plt.bar(bins_corr, heights_norm*K, width=0.2, yerr=err*K,label=r'x: Cauchy($-10$,$10$)',alpha=1,color='forestgreen') # Uso esa integral K para escalear mi distribución
plt.xlabel('x')
plt.ylabel('Frecuencia')
# cauchy teo
K = bins_corr
Cauchy = [cauchy(k,A,B) for k in K]                          # probabilidad
plt.plot(K,Cauchy,label='Cauchy teórica',marker='.',color='red',alpha=0.5)
plt.xlim(-lim,lim)
plt.legend()
plt.grid()
plt.suptitle('Inciso 1 (b)', fontsize=16)
plt.show()



print('Inciso 2: Ejercicio basado en G3P7')###################################################################################################
fig = plt.figure()
#fig.suptitle('Inciso 2: Exponencial', fontsize=16)
# Exponencial
N = 500#000
lamb = 0.25
X = np.random.uniform( 0, 1, N)
Y = - np.log(1-X)/lamb          # Así se consigue una distribución exponencial (explicado en el informe) a partir de uniforme entre 0 y 1 


# Primera parte
binwidth = 1
a = 0
c = np.max(Y)
heights, bins = np.histogram(Y,bins=np.arange(a, c+binwidth, binwidth),range=[a,c])
norm = np.sum(heights) * binwidth
bins_corr = bins[:-1]+binwidth/2
heights_norm = heights/norm
err = np.sqrt(heights)/norm
plt.bar(bins_corr, heights_norm, width=binwidth, yerr=err,label=r'x: Exponencial(0,15)',alpha=1,color='royalblue')
# Teóricai
K = bins_corr
Expo = [exponential(k,lamb) for k in K]                          # probabilidad
plt.plot(K,Expo,label='Exponencial teórica',marker='.',color='red',alpha=0.5)
plt.legend()
plt.grid()
plt.title('Inciso 2: Primera parte')
plt.xlabel('y')
plt.ylabel('Frecuencia')
plt.show()


# Segunda parte
fig = plt.figure()
fig.suptitle('Inciso 2: Segunda parte', fontsize=16)
binwidth_0 = 1
binwidth_1 = 3

# Grafico "mal" primero
ax1 = fig.add_subplot(211)
# bines finos
a = 0
b = 15
c = np.max(Y)
heights, bins = np.histogram(Y,bins=np.arange(a, b+binwidth_0, binwidth_0),range=[a,b])
norm = np.sum(heights) * binwidth_0
bins_corr_0 = bins[:-1]+binwidth_0/2
heights_norm_0 = heights/norm
err_0 = np.sqrt(heights)/norm
ax1.bar(bins_corr_0, heights_norm_0, width=binwidth_0, yerr=err_0,label=r'x: Exponencial(0,15)',alpha=1,color='royalblue')
# bines gruesos
heights, bins = np.histogram(Y,bins=np.arange(b,c, binwidth_1),range=[b,c])
norm_1 = np.sum(heights/4) * binwidth_1
bins_corr_1 = bins[:-1]+binwidth_1/2
heights_norm_1 = heights/norm
err_1 = np.sqrt(heights)/norm
ax1.bar(bins_corr_1, heights_norm_1, width=binwidth_1, yerr=err_1,label=r'x: Exponencial(15,$\infty$)',alpha=1,color='darkorange')
# Teórica
K = list(bins_corr_0)+list(bins_corr_1)
Expo = [exponential(k,lamb) for k in K]                          # probabilidad
ax1.plot(K,Expo,label='Exponencial teórica',marker='.',color='red',alpha=0.5)
ax1.legend()
ax1.grid()
ax1.title.set_text('Sin corrección')
#ax1.set_yscale('log')
ax1.set_xlabel('y')
ax1.set_ylabel('Frecuencia')

# Grafico bien, dividiendo por el ancho de bin
ax2 = fig.add_subplot(212)
# bines finos
ax2.bar(bins_corr_0, heights_norm_0, width=binwidth_0, yerr=err_0,label=r'x: Exponencial(0,15)',alpha=1,color='royalblue')
# bines gruesos
ax2.bar(bins_corr_1, heights_norm_1*binwidth_0/binwidth_1 , width=binwidth_1,   yerr=err_1/binwidth_1,label=r'x: Exponencial(15,$\infty$)',alpha=1,color='darkorange')
# Teórica
ax2.plot(K,Expo,label='Exponencial teórica',marker='.',color='red',alpha=0.5)
ax2.legend()
ax2.grid()
ax2.title.set_text('Corregido')
#ax2.set_yscale('log')
ax2.set_xlabel('y')
ax2.set_ylabel('Frecuencia')

plt.show()
