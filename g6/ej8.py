from random import random
from scipy.stats import norm
import numpy as np
import math
import matplotlib.pyplot as plt

x = np.array([2.00, 2.10, 2.20, 2.30, 2.40, 2.50, 2.60, 2.70, 2.80, 2.90, 3.00])
y = np.array([2.78, 3.29, 3.29, 3.33, 3.23, 3.69, 3.46, 3.87, 3.62, 3.40, 3.99])
N = len(x)
sigma = 0.3

Delta  = N * np.sum(x*x) - math.pow(np.sum(x),2)
cte    = math.pow(sigma,2)/Delta
a1     = ( np.sum(x*x) * np.sum(y) - np.sum(x) * np.sum(x*y) ) / Delta
var_a1 = cte * np.sum(x*x)
a2     = ( N * np.sum(x*y) - np.sum(x)*np.sum(y) ) / Delta
var_a2 = cte * N
cov    = - cte * np.sum(x)
print('a1: '+str(a1))
print('a2: '+str(a2))
print('Varianza a1: '+str(var_a1))
print('Varianza a2: '+str(var_a2))
print('Covarianza a1,a2: '+str(cov))

print('\nAjuste: '+str('{0:.3f}'.format(a1))+" + "+str('{0:.3f}'.format(a2))+" * x")
print(str('a1 = '+'{0:.3f}'.format(a1))+" +- "+str('{0:.3f}'.format(math.pow(var_a1,0.5))))
print(str('a2 = '+'{0:.3f}'.format(a2))+" +- "+str('{0:.3f}'.format(math.pow(var_a2,0.5))))

x_fit = np.linspace(0,5,100)
y_fit = a1 + a2 * x_fit
plt.plot(x_fit,y_fit,label = str('{0:.3f}'.format(a1))+" + "+str('{0:.3f}'.format(a2))+" * x",color='forestgreen')

Y_sup = [] 
Y_inf = [] 
Y_sup_fake = [] 
Y_inf_fake = [] 
for Xa in x_fit:
    sigma_y = math.pow(var_a1 + Xa*Xa*var_a2 + 2*Xa*cov,0.5)
    sigma_y_fake = math.pow(var_a1 + Xa*Xa*var_a2,0.5)
    Y_sup.append( a1 + a2 * Xa + sigma_y)
    Y_inf.append( a1 + a2 * Xa - sigma_y)
    Y_sup_fake.append( a1 + a2 * Xa + sigma_y_fake)
    Y_inf_fake.append( a1 + a2 * Xa - sigma_y_fake)

plt.fill_between(x_fit,Y_inf_fake,Y_sup_fake,alpha=0.1,color='crimson',zorder=1,label='Sin término de Cov')
plt.fill_between(x_fit,Y_inf,Y_sup,alpha=0.2,color='forestgreen',zorder=2,label='Con término de Cov')
plt.errorbar(x,y,yerr=sigma, linestyle="None",label='Datos',marker='o')



plt.xlabel('x')
plt.ylabel('y')
plt.grid()
plt.legend()
plt.show()

