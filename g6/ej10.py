import matplotlib.pyplot as plt
import numpy as np
import math

x  = np.array([10, 8, 13, 9, 11, 14, 6, 4, 12, 7, 5])
y1 = np.array([8.04, 6.95, 7.58, 8.81, 8.33, 9.96, 7.24, 4.26, 10.84, 4.82, 5.68])
y2 = np.array([9.14, 8.14, 8.74, 8.77, 9.26, 8.10, 6.13, 3.10, 9.13, 7.26, 4.74])
y3 = np.array([7.46, 6.77, 12.74, 7.11, 7.81, 8.84, 6.08, 5.39, 8.15, 6.42, 5.73])
x4 = np.array([8, 8, 8, 8, 8, 8, 8, 19, 8, 8, 8])
y4 = np.array([6.58, 5.76, 7.71, 8.84, 8.47, 7.04, 5.25, 12.50, 5.56, 7.91, 6.89])

def ajusteLineal(x,y,sigma):
    N = len(x)
    Delta  = N * np.sum(x*x) - math.pow(np.sum(x),2)
    a1 = ( np.sum(x*x) * np.sum(y) - np.sum(x) * np.sum(x*y) ) / Delta
    a2 = ( N * np.sum(x*y) - np.sum(x)*np.sum(y) ) / Delta
    cte    = math.pow(sigma,2)/Delta
    var_a1 = cte * np.sum(x*x)
    var_a2 = cte * N
    cov    = - cte * np.sum(x)
    return  a1, a2, var_a1, var_a2, cov

sigma = 0.3

A_a1, A_a2, A_var_a1, A_var_a2, A_cov = ajusteLineal(x, y1,sigma)
B_a1, B_a2, B_var_a1, B_var_a2, B_cov = ajusteLineal(x, y2,sigma)
C_a1, C_a2, C_var_a1, C_var_a2, C_cov = ajusteLineal(x, y3,sigma)
D_a1, D_a2, D_var_a1, D_var_a2, D_cov = ajusteLineal(x4,y4,sigma)

fig, axs = plt.subplots(1, 4)

x_fit = np.linspace(min(x)-5,max(x)+5,100)
X_ = [x,x,x,x4]
Y_ = [y1,y2,y3,y4]
a1_ = [A_a1,B_a1,C_a1,D_a1]
a2_ = [A_a2,B_a2,C_a2,D_a2]
var_a1_ = [A_var_a1,B_var_a1,C_var_a1,D_var_a1]
var_a2_ = [A_var_a2,B_var_a2,C_var_a2,D_var_a2]
cov_ = [A_cov,B_cov,C_cov,D_cov]

for ans in range(4):
    axs[ans].errorbar(X_[ans],Y_[ans],yerr=sigma, linestyle="None",label='Datos',marker='o')
    axs[ans].plot(x_fit,A_a1+A_a2*x_fit,label='Anscombe fit '+str(ans),color='forestgreen')
    axs[ans].set_xlim(0,20)
    axs[ans].set_ylim(2,14)

    Y_sup = []
    Y_inf = []
    Y_sup_fake = []
    Y_inf_fake = []
    for Xa in x_fit:
        sigma_y = math.pow(var_a1_[ans] + Xa*Xa*var_a2_[ans] + 2*Xa*cov_[ans],0.5)
        Y_sup.append( a1_[ans] + a2_[ans] * Xa + sigma_y)
        Y_inf.append( a1_[ans] + a2_[ans] * Xa - sigma_y)
    axs[ans].fill_between(x_fit,Y_inf,Y_sup,alpha=0.5,color='forestgreen',label='Incerteza del ajuste')
    axs[ans].grid()
    axs[ans].legend()

plt.show()

