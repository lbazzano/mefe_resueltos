from random import random
from scipy.stats import norm
import numpy as np
import math
import matplotlib.pyplot as plt

x = np.array([2.00, 2.10, 2.20, 2.30, 2.40, 2.50, 2.60, 2.70, 2.80, 2.90, 3.00])
y = np.array([2.78, 3.29, 3.29, 3.33, 3.23, 3.69, 3.46, 3.87, 3.62, 3.40, 3.99])
N = len(x)
Delta  = N * np.sum(x*x) - math.pow(np.sum(x),2)
a1     = ( np.sum(x*x) * np.sum(y) - np.sum(x) * np.sum(x*y) ) / Delta
a2     = ( N * np.sum(x*y) - np.sum(x)*np.sum(y) ) / Delta
sigma = 0.3

cte    = math.pow(sigma,2)/Delta
var_a1 = cte * np.sum(x*x)
var_a2 = cte * N
cov    = - cte * np.sum(x)

X_a = 0.5
sigma_y = math.pow(var_a1 + X_a*X_a*var_a2 + 2*X_a*cov,0.5)
print('Espero en x = '+str(X_a)+' el valor y = '+str(a1 + a2 * X_a)+' +- '+str(sigma_y))



Y_a = []
for n in range(1000):
  y = [] #rescribo y 1000 veces
  for Xa in x:
      y.append(np.random.normal(a1+a2*Xa,sigma))
  # hago el fit a lo mefe:
  Delta  = N * np.sum(x*x) - math.pow(np.sum(x),2)
  a1_temp = ( np.sum(x*x) * np.sum(y) - np.sum(x) * np.sum(x*y) ) / Delta
  a2_temp = ( N * np.sum(x*y) - np.sum(x)*np.sum(y) ) / Delta
  #plt.scatter(x,y,color='blue')
  #plt.plot(np.linspace(0,5,50),a1_temp+a2_temp*np.linspace(0,5,50))
  # guardo lo que vale en 0.5
  Y_a.append(a1_temp + a2_temp * X_a)
#plt.show()
print('Obtengo en x = '+str(X_a)+' el valor y = '+str(np.mean(Y_a))+' +- '+str(np.std(Y_a)))
plt.hist(Y_a,density = True,label='Datos obtenidos')
plt.plot(np.linspace(-1,5,100),norm.pdf(np.linspace(-1,5,100),a1 + a2 * X_a,sigma_y),label='Distribución esperada')
plt.xlabel('y')
plt.ylabel('Frecuencia')
plt.grid()
plt.legend()
plt.show()
