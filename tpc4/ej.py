import matplotlib.pyplot as plt
import numpy as np
import statistics as stats
from nltk import word_tokenize, sent_tokenize
import math
import nltk
import random
from tqdm import tqdm
from numpy.random import choice
from nltk.corpus import stopwords
from nltk.corpus import movie_reviews
from scipy.stats import norm
from math import factorial

def cuentaRachas_python(mezcladas,mediana):
  if mezcladas[0] >= mediana:
    racha_0 = 'up'
  else:
    racha_0 = 'down'
  cuenta_rachas = 1
  for dato in mezcladas[1:]:
    if racha_0 == 'up' and dato < mediana:
      racha_0 = 'down'
      cuenta_rachas += 1
    elif racha_0 == 'down' and dato > mediana:
      racha_0 = 'up'
      cuenta_rachas += 1
  return cuenta_rachas

idiomas = ['Español','Alemán'] # hay que correr con los dos para el cálculo de la potencia. (primero español para definir los Tc y desp aleman para la pot)
plotear = True #False
guardar = False #True
muestras = 100 #50000 # para todos los histogramas (haciendo reposición) menos el primero (t1).

for idioma in idiomas:
  if idioma == 'Español':
    fic = open('./arenal.txt', "r")
    print('\nLeyendo el libro -La Igualdad Social y Política y sus Relaciones con la Libertad- de -Arenal-\t('+str(fic)+')')
    color_idioma = 'steelblue'
  if idioma == 'Alemán':
    fic = open('./schmied.txt', "r")
    print('\nLeyendo el libro -Carlos und Nicolás- de -Schmied-\t('+str(fic)+')')
    color_idioma = 'lightcoral'
  edgecolor_idioma='white'
  lines = []
  for line in fic:
      lines.append(line)
  fic.close()
  N=len(lines)

  # Inciso 1 #################################################################################################################
  print('\n>>Inciso 1 (Estadístico t1): Distribución<<')
  longitudes = []
  for j in range(N):
          longitud = [len(w)for w in word_tokenize(lines[j]) if w.isalpha()]
          for i in longitud:
                  longitudes.append(i)
  print('cantidad de palabras: '+str(len(longitudes)))
  print('media:\t\t'+str(stats.mean(longitudes)))
  print('moda:\t\t'+str(stats.mode(longitudes)))
  print('mediana:\t'+str(stats.median(longitudes)))
  figure, axes = plt.subplots(1,figsize=(10,5))
  bins = np.arange(-0.5,20.5,1)
  ticks = np.arange(0,21,1)
  h,b = np.histogram(longitudes,bins,density=True)
  error = np.sqrt(h/len(longitudes))
  axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
  axes.set_xlabel('Longitud')
  axes.set_ylabel('Entradas')
  axes.set_title('t1 - '+idioma)
  axes.set_xticks(ticks)
  figure.tight_layout()
  if idioma == "Alemán":
    axes.axvline(Tcriticos_esp[0]-0*0.5,label=r'T crítico Español ($\alpha=$'+str(alpha)+')',lw=2,alpha=0.5)
    plt.legend()
  if plotear:
    plt.show()
  if guardar:
    plt.savefig(idioma+'_1.png', bbox_inches='tight')
    plt.close()

  # Inciso 2 #################################################################################################################
  print('\n>>Inciso 2 (Estadístico t20): Estadístico<<')
  n = 20
  m = 0
  mezcladas = longitudes.copy()
  maximos20 = []
  for p in tqdm(range(muestras)):
    maximos20.append(int(max(choice(mezcladas,n))))
  figure, axes = plt.subplots(1,figsize=(10,5))
  bins = np.arange(-0.5,20.5,1)
  ticks = np.arange(0,21,1)
  h,b = np.histogram(maximos20,bins,density=True)
  error = np.sqrt(h/len(maximos20))
  axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
  axes.set_xlabel('Longitud')
  axes.set_ylabel('Entradas')
  axes.set_title('t20 - '+idioma)
  axes.set_xticks(ticks)
  figure.tight_layout()
  if idioma == "Alemán":
    axes.axvline(Tcriticos_esp[1]-0*0.5,label=r'T crítico Español ($\alpha=$'+str(alpha)+')',lw=2,alpha=0.5) 
    plt.legend()
  if plotear:
    plt.show()
  if guardar:
    plt.savefig(idioma+'_2.png', bbox_inches='tight')
    plt.close() 

  # Inciso 3 #################################################################################################################
  if idioma == "Español":
    print('\n>>Inciso 3 (Estadístico t100 español): Test<<')
    # primero repito para 100
    n = 100
    m = 0
    mezcladas = longitudes.copy()
    maximos100 = []
    for p in tqdm(range(muestras)):
      maximos100.append(int(max(choice(mezcladas,n))))
    figure, axes = plt.subplots(1,figsize=(10,5))
    bins = np.arange(-0.5,20.5,1)
    ticks = np.arange(0,21,1)
    h,b = np.histogram(maximos100,bins,density=True)
    error = np.sqrt(h/len(maximos100))
    axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
    axes.set_xlabel('Longitud')
    axes.set_ylabel('Entradas')
    axes.set_title('t100 - '+idioma)
    axes.set_xticks(ticks)
    figure.tight_layout()
    if plotear:
      plt.show()
    if guardar:
      plt.savefig(idioma+'_3.png', bbox_inches='tight')
      plt.close()
    # listo, ahora comparo las tres potencias
    print('\n(rechazo H0: el libro está escrito en español. Test de una cola a la derecha.)')
    Tcriticos_esp = []
    estadisticos = longitudes.copy()
    for estadistico,num in zip([estadisticos,maximos20,maximos100],[1,20,100]):
      m = len(estadistico)
      alpha = 0.05
      estadistico.sort(reverse = True) #integro desde la derecha
      pval = 0
      for est in estadistico:
        pval += 1/m
        if pval > alpha:
            pval -= 1/m
            break
      print('Para t'+str(num)+' = '+str(est)+', p valor = '+str(pval)+'. Entonces rechazo H0 a partir de t'+str(num)+' crítico = '+str(est+1))
      print('Si "mido" t'+str(num)+' = '+str(est)+', no puedo rechazar la hipótesis, ya que la suma de la probabilidad de los bines de '+str(est)+' al máximo es mayor que 0.05')
      Tcriticos_esp.append(est+1)

  # Inciso 4 #################################################################################################################
  if idioma == "Alemán":    
    print('\n>>Inciso 4 (Estadístico t100 alemán): Potencia<<')
    # primero repito para 100
    n = 100
    m = 0
    mezcladas = longitudes.copy()
    maximos100 = []
    for p in tqdm(range(muestras)):
      maximos100.append(int(max(choice(mezcladas,n))))
    figure, axes = plt.subplots(1,figsize=(10,5))
    bins = np.arange(-0.5,20.5,1)
    ticks = np.arange(0,21,1)
    h,b = np.histogram(maximos100,bins,density=True)
    error = np.sqrt(h/len(maximos100))
    axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
    axes.set_xlabel('Longitud')
    axes.set_ylabel('Entradas')
    axes.set_title('t100 - '+idioma)
    axes.set_xticks(ticks)
    figure.tight_layout()
    axes.axvline(Tcriticos_esp[2]-0*0.5,label=r'T crítico Español ($\alpha=$'+str(alpha)+')',lw=2,alpha=0.5)
    plt.legend()
    if plotear:
      plt.show()
    if guardar:
      plt.savefig(idioma+'_4.png', bbox_inches='tight')
      plt.close()
    # listo, ahora comparo las tres potencias
    estadisticos = longitudes.copy()
    for estadistico,num,Tc_esp in zip([estadisticos,maximos20,maximos100],[1,20,100],Tcriticos_esp): # cuando llego a este punto, estos son en alemán :)
      m = len(estadistico)
      beta = 0
      estadistico.sort(reverse = True)
      for est in estadistico:
        beta += 1/m
        if est <= Tc_esp:
            beta -= 1/m
            est -= 1
            break
      print('Para t'+str(num)+' = '+str(est)+',la potencia del test definido antes es = '+str(beta))


  # Inciso 5 #################################################################################################################
  print('\n>>Inciso 5 (Rachas)<<')
  n = 30
  m = 0
  muestra = longitudes.copy()
  rachas30 = []
  for p in tqdm(range(muestras)):
    muestrita = choice(muestra,n)
    mediana = stats.median(muestrita)
    rachas30.append(cuentaRachas_python(muestrita,mediana))
  if idioma == "Alemán":
    estadisticos_der = rachas30.copy()
    estadisticos_izq = rachas30.copy()
    m = len(rachas30)
    alpha = 0.5 # significancia, dos colas (bajo la hipótesis de que en español hay más rachas en gral)
    estadisticos_der.sort(reverse = True)
    estadisticos_izq.sort(reverse = False)
    pval = 0
    for est_izq,est_der in zip(estadisticos_izq,estadisticos_der):
      pval += 2/m
      if pval > alpha:
        pval -= 2/m
        break
    Rcritico_izq = est_izq - 1
    Rcritico_der = est_der + 1
    print('Para la variable aleatoria cantidad de rachas en 30 valores de t1: '+str(est_der)+' < R30 < '+str(est_izq)+', p valor = '+str(pval)+'. Entonces rechazo H0 si caigo afuera de los R30 críticos = '+str(Rcritico_izq)+' , '+str(Rcritico_der))
    print('Por ejemplo, si "mido" R30 = '+str(est_der)+', no puedo rechazar la hipótesis, ya que la suma de la probabilidad de los bines de '+str(est_der)+' al máximo es mayor que '+str(alpha)+ '/2')
  figure, axes = plt.subplots(1,figsize=(10,5))
  bins = np.arange(-0.5,30.5,1)
  ticks = np.arange(0,31,1)
  h,b = np.histogram(rachas30,bins,density=True)
  error = np.sqrt(h/len(rachas30))
  axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
  axes.set_xlabel('Rachas')
  axes.set_ylabel('Entradas')
  axes.set_title('Rachas en 30 t1 - '+idioma)
  axes.set_xticks(ticks)
  figure.tight_layout()
  if idioma == "Alemán":
    axes.axvline(Rcritico_izq+0*0.5,label=r'R30 críticos ($\alpha=$'+str(alpha)+')',lw=2,alpha=0.5,color = 'red')
    axes.axvline(Rcritico_der-0*0.5,lw=2,alpha=0.5,color = 'red')
    # palabras detectadas en un libro real:
    palabras = ['los','vez','en','allí','Edmundo','parecidas','dijo','convencida','paredes','la','palo','es','las','gracioso','cuántas','hiedra','dormir','que','acuerdas','el','sus','puedes','bien','no','sus','bote','que','Pedro','daban','de']
    largos = [len(palabra) for palabra in palabras]
    mediana = stats.median(largos)
    rachasReales = cuentaRachas_python(largos,mediana)
    axes.axvline(rachasReales,label = 'Rachas en libro "físico"',color='forestgreen',alpha=0.5,lw=2,linestyle=':')
    plt.legend()
  if plotear:
    plt.show()
  if guardar:
    plt.savefig(idioma+'_5.png', bbox_inches='tight')
    plt.close()
