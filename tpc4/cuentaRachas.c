#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int *cuentaRachas(int *arr, int n) {
    int mediana = n;
    int c = n;
    int racha_0 = 1;

    if (arr[0] <  mediana) {racha_0 = 0; }
    else {int racha_0 = 1; }

    int cuenta_rachas = 1;
    size_t size = 30;

    for (int i = 1; i < size; ++i) 
    {
      if (racha_0 == 1 && arr[i] < mediana){cuenta_rachas += 1; racha_0 = 0;}
      if (racha_0 == 0 && arr[i] > mediana){cuenta_rachas += 1; racha_0 = 1;}
    }

    return cuenta_rachas;
}
