import matplotlib.pyplot as plt
import numpy as np
import statistics as stats
from nltk import word_tokenize, sent_tokenize
import math
import nltk
import random
from tqdm import tqdm
from numpy.random import choice
#nltk.download('punkt')
#nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.corpus import movie_reviews
from scipy.stats import norm
from math import factorial
def comb(m, n):
  try: 
    res = factorial(m) / (factorial(n) * factorial(m - n))
  except:
    res = 1
  return res

'''
import ctypes as C
cuentaRachas_c = C.CDLL("./cuentaRachas.so")


def cuentaRachas(mezcladas,mediana):
  mediana = int(mediana)
  tupla = tuple(np.array(mezcladas))
  in1 = (C.c_int * len(tupla))(*tupla)
  in2 = (C.c_int * mediana)
  rachas = cuentaRachas_c.cuentaRachas(C.byref(in1),C.c_int(mediana))
  return rachas


print(str(cuentaRachas([2,1,3,4,1],2)))
'''

'''
def cuentaRachas_2(mezcladas,mediana):

  if mezcladas[0] >= mediana:
    racha_0 = 'up'
  if mezcladas[0] < mediana:
    racha_0 = 'down'

  l = 1
  N = 0
  N_up = 0
  N_down = 0
  cuenta_rachas = 1
  cuenta_rachas_up = 0
  cuenta_rachas_down = 0
  same_med = 0
  long_rachas   = []

  for dato in mezcladas[1:]:
    if racha_0 == 'up' and dato > mediana:
      l += 1
      N_up += 1
      N += 1
    elif racha_0 == 'down' and dato < mediana:
      l += 1
      N_down += 1
      N += 1
    elif racha_0 == 'up' and dato < mediana:
      racha_0 = 'down'
      long_rachas.append(l)
      cuenta_rachas += 1
      cuenta_rachas_up += 1
      l = 1
      N_up += 1
      N += 1
    elif racha_0 == 'down' and dato > mediana:
      racha_0 = 'up'
      long_rachas.append(l)
      cuenta_rachas += 1
      cuenta_rachas_down += 1
      l = 1
      N_down += 1
      N += 1
    elif dato == mediana:
      same_med += 1
    else:
      print('ERROR')

  return cuenta_rachas
'''


def cuentaRachas_python(mezcladas,mediana):
  if mezcladas[0] >= mediana:
    racha_0 = 'up'
  else:
    racha_0 = 'down'
  cuenta_rachas = 1
  for dato in mezcladas[1:]:
    if racha_0 == 'up' and dato < mediana:
      racha_0 = 'down'
      cuenta_rachas += 1
    elif racha_0 == 'down' and dato > mediana:
      racha_0 = 'up'
      cuenta_rachas += 1
  return cuenta_rachas


idiomas = ['Español','Alemán'] # hay que correr con los dos para el cálculo de la potencia. (primero español para definir los Tc y desp aleman para la pot)

for idioma in idiomas:
  if idioma == 'Español':
    #fic = open('./gorriti.txt', "r")
    fic = open('./arenal.txt', "r")
    #print('\nLeyendo el libro Oasis en la Vida de Gorriti\t('+str(fic)+')')
    print('\nLeyendo el libro -La Igualdad Social y Política y sus Relaciones con la Libertad- de -Arenal-\t('+str(fic)+')')
    color_idioma = 'steelblue'
    edgecolor_idioma='white'
  if idioma == 'Alemán':
    fic = open('./schmied.txt', "r")
    print('\nLeyendo el libro -Carlos und Nicolás- de -Schmied-\t('+str(fic)+')')
    color_idioma = 'lightcoral'
    edgecolor_idioma='white'
  lines = []
  for line in fic:
      lines.append(line)
  fic.close()
  N=len(lines)

  # Inciso 1 #################################################################################################################
  print('\n>>Inciso 1 (Estadístico t1)<<')
  longitudes = []
  for j in range(N):
          longitud = [len(w)for w in word_tokenize(lines[j]) if w.isalpha()]
          for i in longitud:
                  longitudes.append(i)
  print('media:\t\t'+str(stats.mean(longitudes)))
  print('mediana:\t'+str(stats.median(longitudes)))
  print('moda:\t\t'+str(stats.mode(longitudes)))
  
  figure, axes = plt.subplots(1,figsize=(10,5))
  bins = np.arange(-0.5,20.5,1)
  ticks = np.arange(0,21,1)
  h,b = np.histogram(longitudes,bins,density=True)
  error = np.sqrt(h/len(longitudes))
  axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
  axes.set_xlabel('Longitud')
  axes.set_ylabel('Entradas')
  axes.set_title('t1 - '+idioma)
  axes.set_xticks(ticks)
  figure.tight_layout()
  if idioma == "Alemán":
    axes.axvline(Tcriticos_esp[0]-0.5,label=r'T crítico ($\alpha=$'+str(alpha)+')',lw=2,alpha=0.5)
    plt.legend()
  #plt.show()
  plt.savefig(idioma+'_1.png', bbox_inches='tight')
  plt.close()

  # Inciso 2 #################################################################################################################
  print('\n>>Inciso 2 (Estadístico t20)<<')
  n = 20
  muestras = 500000 # para hacer reposición.
  m = 0
  mezcladas = longitudes.copy()
  maximos20 = []
  for p in tqdm(range(muestras)):
    maximos20.append(int(max(choice(mezcladas,n))))
  '''
  while len(mezcladas)>= n and m <= muestras:
    random.shuffle(mezcladas) # si elijo palabras al azar
    muestra = mezcladas[:n]
    maximos20.append(max(muestra))
    #mezcladas = mezcladas[n:]    # Si quiero tener reposición, comento esta línea
    print('Porcentaje del libro leído: '+str(100*(1-len(mezcladas)/len(longitudes)))+'\t%', end = " \t- Cantidad de muestras: "+str(m)+" \r")
    m += 1
  '''
  print('\nmedia:\t\t'+str(stats.mean(maximos20)))
  print('mediana:\t'+str(stats.median(maximos20)))
  print('moda:\t\t'+str(stats.mode(maximos20))) # si esto tira error es porque son pocas entradas y hay dos máximos, menos porbable aumentando "muestras"
  
  figure, axes = plt.subplots(1,figsize=(10,5))
  bins = np.arange(-0.5,20.5,1)
  ticks = np.arange(0,21,1)
  h,b = np.histogram(maximos20,bins,density=True)
  error = np.sqrt(h/len(maximos20))
  axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
  axes.set_xlabel('Longitud')
  axes.set_ylabel('Entradas')
  axes.set_title('t20 - '+idioma)
  axes.set_xticks(ticks)
  figure.tight_layout()
  if idioma == "Alemán":
    axes.axvline(Tcriticos_esp[1]-0.5,label=r'T crítico ($\alpha=$'+str(alpha)+')',lw=2,alpha=0.5) 
    plt.legend()
  #plt.show()
  plt.savefig(idioma+'_2.png', bbox_inches='tight')
  plt.close()
  

  # Inciso 3 #################################################################################################################
  if idioma == "Español":
    print('\n>>Inciso 3 (Estadístico t100 español)<<')
    # primero repito para 100
    n = 100
    m = 0
    mezcladas = longitudes.copy()
    maximos100 = []
    for p in range(muestras):
      maximos100.append(int(max(choice(mezcladas,n))))
    '''
    while len(mezcladas)>= n and m <= muestras:
      random.shuffle(mezcladas)
      muestra = mezcladas[:n]
      maximos100.append(max(muestra))
      #mezcladas = mezcladas[n:]    # Si quiero tener reposición, comento esta línea
      print('Porcentaje del libro leído: '+str(100*(1-len(mezcladas)/len(longitudes)))+'\t%', end = " \t- Cantidad de muestras: "+str(m)+" \r")
      m += 1
    '''
    figure, axes = plt.subplots(1,figsize=(10,5))
    bins = np.arange(-0.5,20.5,1)
    ticks = np.arange(0,21,1)
    h,b = np.histogram(maximos100,bins,density=True)
    error = np.sqrt(h/len(maximos100))
    axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
    axes.set_xlabel('Longitud')
    axes.set_ylabel('Entradas')
    axes.set_title('t100 - '+idioma)
    axes.set_xticks(ticks)
    figure.tight_layout()
    #plt.show()
    plt.savefig(idioma+'_3.png', bbox_inches='tight')
    plt.close()
    # listo, ahora comparo los tres poderes

    print('\n>>Inciso 3 (rechazo de H0: el libro está escrito en español. Test de una cola a la derecha.)<<')
    Tcriticos_esp = []
    estadisticos = longitudes.copy()
    for estadistico,num in zip([estadisticos,maximos20,maximos100],[1,20,100]):
      m = len(estadistico)
      alpha = 0.05
      estadistico.sort(reverse = True)
      pval = 0
      for est in estadistico:
        pval += 1/m
        if pval > alpha:
            pval -= 1/m
            break
      print('Para t'+str(num)+' = '+str(est)+', p valor = '+str(pval)+'. Entonces rechazo H0 a partir de t'+str(num)+' crítico = '+str(est+1))
      print('Si "mido" t'+str(num)+' = '+str(est)+', no puedo rechazar la hipótesis, ya que la suma de la probabilidad de los bines de '+str(est)+' al máximo es mayor que 0.05')
      Tcriticos_esp.append(est+1)

  # Inciso 4 #################################################################################################################
  if idioma == "Alemán":
    
    print('\n>>Inciso 4 (Estadístico t100 alemán)<<')
    # primero repito para 100
    n = 100
    m = 0
    mezcladas = longitudes.copy()
    maximos100 = []
    for p in range(muestras):
      maximos100.append(int(max(choice(mezcladas,n))))
    '''
    while len(mezcladas)>= n and m <= muestras:
      random.shuffle(mezcladas)
      muestra = mezcladas[:n]
      maximos100.append(max(muestra))
      #mezcladas = mezcladas[n:]    # Si quiero tener reposición, comento esta línea
      print('Porcentaje del libro leído: '+str(100*(1-len(mezcladas)/len(longitudes)))+'\t%', end = " \t- Cantidad de muestras: "+str(m)+" \r")
      m += 1
    '''
    figure, axes = plt.subplots(1,figsize=(10,5))
    bins = np.arange(-0.5,20.5,1)
    ticks = np.arange(0,21,1)
    h,b = np.histogram(maximos100,bins,density=True)
    error = np.sqrt(h/len(maximos100))
    axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
    axes.set_xlabel('Longitud')
    axes.set_ylabel('Entradas')
    axes.set_title('t100 - '+idioma)
    axes.set_xticks(ticks)
    figure.tight_layout()
    axes.axvline(Tcriticos_esp[2]-0.5,label=r'T crítico ($\alpha=$'+str(alpha)+')',lw=2,alpha=0.5)

    plt.legend()
    #plt.show()
    plt.savefig(idioma+'_4.png', bbox_inches='tight')
    plt.close()

    # listo, ahora comparo los tres poderes

    print('\n>>Inciso 4 (Potencia)<<')
    estadisticos = longitudes.copy()
    for estadistico,num,Tc_esp in zip([estadisticos,maximos20,maximos100],[1,20,100],Tcriticos_esp): # cuando llego a este punto, estos son en alemán :)
      m = len(estadistico)
      beta = 0
      estadistico.sort(reverse = True)
      for est in estadistico:
        beta += 1/m
        if est <= Tc_esp:
            beta -= 1/m
            est -= 1
            break
      print('Para t'+str(num)+' = '+str(est)+',la potencia del test definido antes es = '+str(beta))


  # Inciso 5 #################################################################################################################
  print('\n>>Inciso 5 (Rachas)<<')
  mediana = stats.median(longitudes)
  if idioma == "Alemán":
    mediana_aleman = mediana
  print('mediana:\t'+str(mediana))

  n = 30
  m = 0
  muestra = longitudes.copy()
  rachas30 = []
  for p in range(muestras):
    muestrita = choice(muestra,n)
    mediana = stats.median(muestrita)
    rachas30.append(cuentaRachas_python(muestrita,mediana))

  '''
  while len(muestra)>= n and m <= muestras:
    random.shuffle(muestra) # para mezclar antes
    muestrita = muestra[:n]  
    mediana = stats.median(muestrita) #!!!!!!!!!!
    rachas30.append(cuentaRachas(muestrita,mediana))
    #rachas30.append(cuentaRachas_python(muestrita,mediana))
    #muestra = muestra[n:]
    print('Porcentaje del libro leído: '+str(100*(1-len(muestra)/len(longitudes)))+'\t%', end = " \t- Cantidad de muestras: "+str(m)+" \r")
    m += 1
  '''
  print('\nmedia:\t\t'+str(stats.mean(rachas30)))
  print('mediana:\t'+str(stats.median(rachas30)))
  #print('moda:\t\t'+str(stats.mode(rachas30))) # si esto tira error es porque son pocas entradas y hay dos máximos, menos porbable aumentando "muestras"
  
  if idioma == "Alemán":
    estadisticos_der = rachas30.copy()
    estadisticos_izq = rachas30.copy()

    m = len(estadisticos)
    alpha = 0.05 # significancia, una cola a la derecha (bajo la hipótesis de que en español hay más rachas en gral)
    estadisticos_der.sort(reverse = True)
    estadisticos_izq.sort(reverse = False)
    pval = 0
    for est_izq,est_der in zip(estadisticos_izq,estadisticos_der):
      pval += 2/m
      if pval > alpha:
        pval -= 2/m
        break
    Rcritico_izq = est_izq - 1
    Rcritico_der = est_der + 1
    print('Para la variable aleatoria cantidad de rachas en 30 valores de t1: '+str(est_der)+' < R30 < '+str(est_izq)+', p valor = '+str(pval)+'. Entonces rechazo H0 si caigo afuera de los R30 críticos = '+str(Rcritico_izq)+' , '+str(Rcritico_der))
    print('Por ejemplo, si "mido" R30 = '+str(est_der)+', no puedo rechazar la hipótesis, ya que la suma de la probabilidad de los bines de '+str(est_der)+' al máximo es mayor que '+str(alpha)+ '/2')

  figure, axes = plt.subplots(1,figsize=(10,5))
  bins = np.arange(-0.5,30.5,1)
  ticks = np.arange(0,31,1)
  h,b = np.histogram(rachas30,bins,density=True)
  error = np.sqrt(h/len(rachas30))
  axes.bar(b[:-1]+0.5,height=h,width =1,color=color_idioma,edgecolor=edgecolor_idioma,yerr=error,capsize=0)
  axes.set_xlabel('Rachas')
  axes.set_ylabel('Entradas')
  axes.set_title('Rachas en 30 t1 - '+idioma)
  axes.set_xticks(ticks)
  figure.tight_layout()
  if idioma == "Alemán":
    axes.axvline(Rcritico_izq+0.5,label=r'R30 críticos ($\alpha=$'+str(alpha)+')',lw=2,alpha=0.5)
    axes.axvline(Rcritico_der-0.5,lw=2,alpha=0.5)

    # palabras detectadas en un libro real:
    palabras = ['los','vez','en','allí','Edmundo','parecidas','dijo','convencida','paredes','la','palo','es','las','gracioso','cuántas','hiedra','dormir','que','acuerdas','el','sus','puedes','bien','no','sus','bote','que','Pedro','daban','de']
    largos = [len(palabra) for palabra in palabras]
    mediana = stats.median(largos) #!!!!!!!!!!
    #rachasNarnia = cuentaRachas(largos,mediana)
    rachasNarnia = cuentaRachas_python(largos,mediana)
    axes.axvline(rachasNarnia,label = 'Rachas en libro "físico"',color='forestgreen',alpha=0.5,lw=2)    
    plt.legend()
  #plt.show()
  plt.savefig(idioma+'_5.png', bbox_inches='tight')
  plt.close()


# las rachas = 1 vienen de cosas como:
# [6, 9, 2, 2, 5, 2, 4, 7, 5, 2, 4, 2, 2, 2, 2, 7, 6, 6, 2, 6, 2, 2, 2, 6, 2, 7, 2, 5, 2, 2]
# donde la mediana es 2 y el resto de los valores queda por encima, entonces es una racha
'''


  # proba "esperada" normal
  n = N_up
  m = N_down
  mu = 1+ 2*n*m/(n+m)
  var = 2*n*m * (2*n*m - m - n) / ((n+m) * (n+m) * (n+m-1))
  sigma = math.sqrt(var)
  x = np.linspace(0,30, 1000)
  figure, axes = plt.subplots(1,figsize=(10,5))
  axes.plot(x, norm.pdf(x, mu, sigma),color_idioma, lw=5, alpha=0.6, label='norm pdf')
  print('Cantidad de rachas observadas:'+str(cuenta_rachas))
  print('Normal aproximada: N('+str(mu)+' , '+str(sigma)+')')

  integral = norm.cdf(cuenta_rachas, mu, sigma)
  if integral > 0.5: # cola derecha
    pvalor = (1 - integral)/2
  else: # cola izquierda
    pvalor = integral/2
  print('pvalor = '+str(pvalor))
  CL = 0.6
  print('Rachas críticas para un nivel de confianza de '+str(CL)+' = '+str(norm.ppf((1-CL)/2, mu, sigma)))

  axes.set_xlabel('Número de rachas')
  axes.set_ylabel('Probabilidad')
  axes.set_title(idioma)
  figure.tight_layout()
  axes.axvline(cuenta_rachas,label='Rachas observadas en libro virtual ('+idioma+')',color='black')
  axes.axvline(norm.ppf(1-CL/2, mu, sigma),label='valor crítico',color=color_idioma)
  axes.axvline(norm.ppf(CL/2, mu, sigma),color=color_idioma)
  axes.legend()
  axes.set_xlim(0,30)
  axes.set_ylim(0,0.25)
  if idioma == "Español":
    plt.show()

  
  if idioma == "Alemán":
  
    # palabras detectadas en un libro real:
    palabras = ['los','vez','en','allí','Edmundo','parecidas','dijo','convencida','paredes','la','palo','es','las','gracioso','cuántas','hiedra','dormir','que','acuerdas','el','sus','puedes','bien','no','sus','bote','que','Pedro','daban','de']
    largos = [len(palabra) for palabra in palabras]

    mezcladas = largos
    if mezcladas[0] >= mediana:
      racha_0 = 'up'
    if mezcladas[0] < mediana:
      racha_0 = 'down'

    l = 1
    N = 0
    N_up = 0
    N_down = 0
    cuenta_rachas = 0
    cuenta_rachas_up = 0
    cuenta_rachas_down = 0
    same_med = 0
    long_rachas   = []
    for dato in mezcladas[1:]:
      if racha_0 == 'up' and dato > mediana:
        l += 1
        N_up += 1
        N += 1
      elif racha_0 == 'down' and dato < mediana:
        l += 1
        N_down += 1
        N += 1
      elif racha_0 == 'up' and dato < mediana:
        racha_0 = 'down'
        long_rachas.append(l)
        cuenta_rachas += 1
        cuenta_rachas_up += 1
        l = 1
        N_up += 1
        N += 1
      elif racha_0 == 'down' and dato > mediana:
        racha_0 = 'up'
        long_rachas.append(l)
        cuenta_rachas += 1
        cuenta_rachas_down += 1
        l = 1
        N_down += 1
        N += 1
      elif dato == mediana:
        same_med += 1
      else:
        print('ERROR')
  
    integral = norm.cdf(cuenta_rachas, mu, sigma) # mu y sigma de la hipotesis nula Alemana
    if integral > 0.5: # cola derecha
      pvalor = (1 - integral)/2
    else: # cola izquierda
      pvalor = integral/2
    print(pvalor)
    print( norm.ppf((1-CL)/2, mu, sigma))

    axes.axvline(cuenta_rachas,label='Rachas observadas en libro real (Español)',color='forestgreen')
    axes.legend()
    plt.show()





  # esto a veces anda bien pero a veces grafica cualquier cosa, voy con la aproximación gaussiana que va re bien.
  
  y2 = []
  y3 = []
  x2 = []
  x3 = []
  for k in range(int(2),int(30)):
    print(n,m,k)
    print(comb(n-1,k-1))
    print(comb(m-1,k-1))
    y2.append(2* comb(n-1,k-1)*comb(m-1,k-1) /comb(n+m,n))
    y3.append((comb(n-1,k-2)*comb(m-1,k-1)+comb(n-1,k-1)*comb(m-1,k-2)) /comb(n+m,n))
    x2.append(2*k)
    x3.append(2*k-1)
    #print(2* comb(n-1,k-1)*comb(m-1,k-1) /comb(n+m,n),(comb(n-1,k-2)*comb(m-1,k-1)+comb(n-1,k-1)*comb(m-1,k-2)) /comb(n+m,n),k)
  axes.plot(x2,y2,'o')
  axes.plot(x3,y3,'o')
''' 
