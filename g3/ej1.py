from scipy.integrate import quad
import math
import numpy as np

def gauss(x,mu,sigma):
    A = 1 / (sigma * math.sqrt(2*math.pi))
    return A * math.exp(-(math.pow((x - mu)/sigma,2))/2)

mu = 175
sigma = 8
I = quad(gauss,0,195, args=(mu,sigma))

print'Intgral: '+str((I[0]))
