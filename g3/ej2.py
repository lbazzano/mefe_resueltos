from scipy.integrate import quad
import math
import numpy as np

def gauss(x,mu,sigma):
    A = 1 / (sigma * math.sqrt(2*math.pi))
    return A * math.exp(-(math.pow((x - mu)/sigma,2))/2)

print('Inciso a')
mu = 0
sigma = 1
a = 0
f = 0
while f <= 0.9:
  F = quad(gauss,-a,a, args=(mu,sigma))
  f = F[0]
  a += 0.0001 

print'a: '+str(a)
print'Intgral: '+str((F[0]))


