from scipy.integrate import quad
import math
import numpy as np
import matplotlib.pyplot as plt

mu, sigma = 0, 1 # mean and standard deviation
X = np.random.normal(mu, sigma, 100000)

a = 9
b = 6
Y = a * X + b
plt.hist(X,bins=100,label='mu=0, sigma=1',density=True)
plt.hist(Y,bins=100,label='mu='+str(b)+', sigma='+str(a),density=True,alpha=0.5)
plt.legend()
plt.show()


