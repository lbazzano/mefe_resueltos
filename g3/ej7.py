from scipy.integrate import quad
import math
import numpy as np
import matplotlib.pyplot as plt

X = np.random.uniform( 0, 1, 10000000)
Y = np.exp(X)
X_ = np.random.uniform( 0, 1, 10000000)
Y_ = np.log(X_)
e = 5
plt.hist(X,bins=1001,range=[-e,e],label='X -> Uniform(0,1)',density=True)
plt.hist(Y,bins=1001,range=[-e,e],label='Y -> Exp(X)',density=True,alpha=0.5)
plt.hist(Y_,bins=1001,range=[-e,e],label='X -> Ln(X)',density=True,alpha=0.5)
plt.legend()
plt.show()


