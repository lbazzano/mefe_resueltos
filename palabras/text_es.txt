Debo a la conjunción de un espejo y de una enciclopedia el descubrimiento de Uqbar. El
espejo inquietaba el fondo de un corredor en una quinta de la calle Gaona, en Ramos
Mejía; la enciclopedia falazmente se llama The Anglo American Cyclopaedia (Nueva York,
1917) y es una reimpresión literal, pero también morosa, de la Encyclopaedia Britannica
de 1902. El hecho se produjo hará unos cinco años. Bioy Casares había cenado conmigo
esa noche y nos demoró una vasta polémica sobre la ejecución de una novela en primera
persona, cuyo narrador omitiera o desfigurara los hechos e incurriera en diversas
contradicciones, que permitieran a unos pocos lectores -a muy pocos lectores- la
adivinación de una realidad atroz o banal. Desde el fondo remoto del corredor, el espejo
nos acechaba. Descubrimos (en la alta noche ese descubrimiento es inevitable) que los
espejos tienen algo monstruoso. Entonces Bioy Casares recordó que uno de los
heresiarcas de Uqbar había declarado que los espejos y la cópula son abominables,
porque multiplican el número de los hombres. Le pregunté el origen de esa memorable
sentencia y me contestó que The Anglo American Cyclopaedia la registraba, en su
artículo sobre Uqbar. La quinta (que habíamos alquilado amueblada) poseía un ejemplar
de esa obra. En las últimas páginas del volumen XLVI dimos con un artículo sobre Upsala;
en las primeras del XLVII, con uno sobre Ural - Altaic Languages, pero ni una palabra
sobre Uqbar. Bioy, un poco azorado, interrogó los tomos del índice. Agotó en vano todas
las lecciones imaginables: Ukbar, Ucbar, Ooqbar, Ookbar, Oukbahr... Antes de irse, me
dijo que era una región del Irak o del Asia Menor. Confieso que asentí con alguna
incomodidad. Conjeturé que ese país indocumentado y ese heresiarca anónimo eran una
ficción improvisada por la modestia de Bioy para justificar una frase. El examen estéril de
uno de los atlas de Justus Perthes fortaleció mi duda.
Al día siguiente, Bioy me llamó desde Buenos Aires. Me dijo que tenía a la vista el
artículo sobre Uqbar, en el volumen XLVI de la Enciclopedia. No constaba el nombre del
heresiarca, pero sí la noticia de su doctrina, formulada en palabras casi idénticas a las
repetidas por él, aunque -tal vez- literariamente inferiores. Él había recordado:
Copulation and mirrors are abominable. El texto de la Enciclopedia decía: «Para uno de
esos gnósticos, el visible universo era una ilusión o (más precisamente) un sofisma. Los
espejos y la paternidad son abominables (mirrors and fatherhood are abominable)
porque lo multiplican y lo divulgan». Le dije, sin faltar a la verdad, que me gustaría ver
ese artículo. A los pocos días lo trajo. Lo cual me sorprendió, porque los escrupulosos
índices cartográficos de la Erdkunde de Ritter ignoraban con plenitud el nombre de
Uqbar.

Leímos con algún cuidado el artículo. El pasaje recordado por Bioy era tal vez el único
sorprendente. El resto parecía muy verosímil, muy ajustado al tono general de la obra y
(como es natural) un poco aburrido. Releyéndolo, descubrimos bajo su rigurosa escritura
una fundamental vaguedad. De los catorce nombres que figuraban en la parte geográfica,
sólo reconocimos tres -Jorasán, Armenia, Erzerum-, interpolados en el texto de un modo
ambiguo. De los nombres históricos, uno solo: el impostor Esmerdis el mago, invocado
más bien como una metáfora. La nota parecía precisar las fronteras de Uqbar, pero sus
nebulosos puntos de referencia eran ríos y cráteres y cadenas de esa misma región.
Leímos, verbigracia, que las tierras bajas de Tsai Jaldún y el delta del Axa definen la
frontera del sur y que en las islas de ese delta procrean los caballos salvajes. Eso, al
principio de la página 918. En la sección histórica (página 920) supimos que a raíz de las
persecuciones religiosas del siglo XIII, los ortodoxos buscaron amparo en las islas, donde
perduran todavía sus obeliscos y donde no es raro exhumar sus espejos de piedra. La
sección «Idioma y literatura» era breve. Un solo rasgo memorable: anotaba que la
literatura de Uqbar era de carácter fantástico y que sus epopeyas y sus leyendas no se
referían jamás a la realidad, sino a las dos regiones imaginarias de Mlejnas y de Tlön... La
bibliografía enumeraba cuatro volúmenes que no hemos encontrado hasta ahora, aunque
el tercero -Silas Haslam: Hystory of the Land Called Uqbar, 1874- figura en los catálogos
de librería de Bernard Quaritch 1 . El primero, Lesbare und lesenswerthe Bemerkungen
über das Land Ukkbar in Klein-Asien, data de 1641 y es obra de Johannes Valentinus
Andreä. El hecho es significativo; un par de años después, di con ese nombre en las
inesperadas páginas de De Quincey (Writings, decimotercer volumen) y supe que era el
de un teólogo alemán que a principios del siglo XVII describió la imaginaria comunidad de
la Rosa-Cruz -que otros luego fundaron, a imitación de lo prefigurado por él.
Esta noche visitamos la Biblioteca Nacional. En vano fatigamos atlas, catálogos, anuarios
de sociedades geográficas, memorias de viajeros e historiadores: nadie había estado
nunca en Uqbar. El índice general de la enciclopedia de Bioy tampoco registraba ese
nombre. Al día siguiente, Carlos Mastronardi (a quien yo había referido el asunto) advirtió
en una librería de Corrientes y Talcahuano los negros y dorados lomos de la Anglo
American Cyclopaedia... Entró e interrogó el volumen XLVI. Naturalmente, no dio con el
menor indicio de Uqbar.

Lo recuerdo en el corredor del hotel, con un libro de matemáticas en la
mano, mirando a veces los colores irrecuperables del cielo. Una tarde, hablamos del
sistema duodecimal de numeración (en el que doce se escribe 10). Ashe dijo que
precisamente estaba trasladando no sé qué tablas duodecimales a sexagesimales (en las
que sesenta se escribe 10). Agregó que ese trabajo le había sido encargado por un
noruego: en Rio Grande do Sul. Ocho años que lo conocíamos y no había mencionado
nunca su estadía en esa región... Hablamos de vida pastoril, de capangas, de la
etimología brasilera de la palabra gaucho (que algunos viejos orientales todavía
pronuncian gaúcho) y nada más se dijo -Dios me perdone- de funciones duodecimales. En
septiembre de 1937 (no estábamos nosotros en el hotel) Herbert Ashe murió de la rotura
de un aneurisma. Días antes, había recibido del Brasil un paquete sellado y certificado.
Era un libro en octavo mayor. Ashe lo dejó en el bar, donde -meses después- lo encontré.
Me puse a hojearlo y sentí un vértigo asombrado y ligero que no describiré, porque ésta
no es la historia de mis emociones sino de Uqbar y Tlön y Orbis Tertius. En una noche del
Islam que se llama la Noche de las Noches se abren de par en par las secretas puertas del
cielo y es más dulce el agua en los cántaros; si esas puertas se abrieran, no sentiría lo
que en esa tarde sentí. El libro estaba redactado en inglés y lo integraban 1001 páginas.
En el amarillo lomo de cuero leí estas curiosas palabras que la falsa carátula repetía: A
First Encyclopaedia of Tlön. Vol XI. Hlaer to jangr. No había indicación de fecha ni de
lugar. En la primera página y en una hoja de papel de seda que cubría una de las láminas
en colores había estampado un óvalo azul con esta inscripción: Orbis Tertius. Hacía dos
años que yo había descubierto en un tomo de cierta enciclopedia pirática una somera
descripción de un falso país; ahora me deparaba el azar algo más precioso y más arduo.
Ahora tenía en las manos un vasto fragmento metódico de la historia total de un planeta
desconocido, con sus arquitecturas y sus barajas, con el pavor de sus mitologías y el
rumor de sus lenguas, con sus emperadores y sus mares, con sus minerales y sus pájaros
y sus peces, con su álgebra y su fuego, con su controversia teológica y metafísica. Todo
ello articulado, coherente, sin visible propósito doctrinal o tono paródico.
En el onceno tomo de que hablo hay alusiones a tomos ulteriores y precedentes. Néstor
Ibarra, en un artículo ya clásico de la NRF, ha negado que existen esos aláteres; Ezequiel
Martínez Estrada y Drieu la Rochelle han refutado, quizá victoriosamente, esa duda. El
hecho es que hasta ahora las pesquisas más diligentes han sido estériles. En vano hemos
desordenado las bibliotecas de las dos Américas y de Europa. Alfonso Reyes, harto de
esas fatigas subalternas de índole policial, propone que entre todos acometamos la obra
de reconstruir los muchos y macizos tomos que faltan: ex ungue leonem. Calcula, entre
veras y burlas, que una generación de tlönistas puede bastar. Ese arriesgado cómputo
nos retrae al problema fundamental: ¿Quiénes inventaron a Tlön? El plural es inevitable,
porque la hipótesis de un solo inventor -de un infinito Leibniz obrando en la tiniebla y en
la modestia- ha sido descartada unánimemente. Se conjetura que este brave new world
es obra de una sociedad secreta de astrónomos, de biólogos, de ingenieros, de
metafísicos, de poetas, de químicos, de algebristas, de moralistas, de pintores, de
geómetras... dirigidos por un oscuro hombre de genio. Abundan individuos que dominan
esas disciplinas diversas, pero no los capaces de invención y menos los capaces de
subordinar la invención a un riguroso plan sistemático. Ese plan es tan vasto que la
contribución de cada escritor es infinitesimal. Al principio se creyó que Tlön era un mero
caos, una irresponsable licencia de la imaginación; ahora se sabe que es un cosmos y las
íntimas leyes que lo rigen han sido formuladas, siquiera en modo provisional. Básteme
recordar que las contradicciones aparentes del onceno tomo son la piedra fundamental de
la prueba de que existen los otros: tan lúcido y tan justo es el orden que se ha

Hume notó para siempre que los argumentos de Berkeley no admitían la menor réplica
y no causaban la menor convicción. Ese dictamen es del todo verídico en su aplicación a la
tierra; del todo falso en Tlön. Las naciones de ese planeta son –congénitamente-
idealistas. Su lenguaje y las derivaciones de su lenguaje -la religión, las letras, la
metafísica- presuponen el idealismo. El mundo para ellos no es un concurso de objetos en
el espacio; es una serie heterogénea de actos independientes. Es sucesivo, temporal, no
espacial. No hay sustantivos en la conjetural Ursprache de Tlön, de la que proceden los
idiomas «actuales» y los dialectos: hay verbos impersonales, calificados por sufijos (o
prefijos) monosilábicos de valor adverbial. Por ejemplo: no hay palabra que corresponda a
la palabra luna, pero hay un verbo que sería en español lunecer o lunar. «Surgió la luna
sobre el río» se dice « hlör u fang axaxaxas mlö » o sea en su orden: «hacia arriba
(upward) detrás duradero-fluir luneció».

Lo anterior se refiere a los idiomas del hemisferio austral. En los del hemisferio boreal
(de cuya Ursprache hay muy pocos datos en el onceno tomo) la célula primordial no es el
verbo, sino el adjetivo monosilábico. El sustantivo se forma por acumulación de adjetivos.
No se dice luna: se dice aéreo-claro sobre oscuro-redondo o anaranjado-tenue-del cielo o
cualquier otra agregación. En el caso elegido la masa de adjetivos corresponde a un
objeto real; el hecho es puramente fortuito. En la literatura de este hemisferio (como en
el mundo subsistente de Meinong) abundan los objetos ideales, convocados y disueltos en
un momento, según las necesidades poéticas. Los determina, a veces, la mera
simultaneidad. Hay objetos compuestos de dos términos, uno de carácter visual y otro
auditivo: el color del naciente y el remoto grito de un pájaro. Los hay de muchos: el sol y
el agua contra el pecho del nadador, el vago rosa trémulo que se ve con los ojos cerrados,
la sensación de quien se deja llevar por un río y también por el sueño. Esos objetos de
segundo grado pueden combinarse con otros; el proceso, mediante ciertas abreviaturas,
es prácticamente infinito. Hay poemas famosos compuestos de una sola enorme palabra.
Esta palabra integra un objeto poético creado por el autor. El hecho de que nadie crea en
la realidad de los sustantivos hace, paradójicamente, que sea interminable su número.
Los idiomas del hemisferio boreal de Tlön poseen todos los nombres de las lenguas
indoeuropeas y otros muchos más.

de nombrarlo - id est, de clasificarlo- importa un falseo. De ello cabría deducir que no hay
ciencias en Tlön -ni siquiera razonamientos. La paradójica verdad es que existen, en casi
innumerable número. Con las filosofías acontece lo que acontece con los sustantivos en el
hemisferio boreal. El hecho de que toda filosofía sea de antemano un juego dialéctico, una
Philosophie des Als Ob, ha contribuido a multiplicarlas. Abundan los sistemas increíbles,
pero de arquitectura agradable o de tipo sensacional. Los metafísicos de Tlön no buscan la
verdad ni siquiera la verosimilitud: buscan el asombro. Juzgan que la metafísica es una
rama de la literatura fantástica. Saben que un sistema no es otra cosa que la
subordinación de todos los aspectos del universo a uno cualquiera de ellos. Hasta la frase
«todos los aspectos» es rechazable, porque supone la imposible -adición del instante
presente y de los pretéritos. Tampoco es lícito el plural «los pretéritos», porque supone
otra operación imposible... Una de las escuelas de Tlön llega a negar el tiempo: razona
que el presente es indefinido, que el futuro no tiene realidad sino como esperanza
presente, que el pasado no tiene realidad sino como recuerdo presente. 1 Otra escuela
declara que ha transcurrido ya todo el tiempo y que nuestra vida es apenas el recuerdo o
reflejo crepuscular, y sin duda falseado y mutilado, de un proceso irrecuperable. Otra, que
la historia del universo -y en ellas nuestras vidas y el más tenue detalle de nuestras
vidas- es la escritura que produce un dios subalterno para entenderse con un demonio.
Otra, que el universo es comparable a esas criptografías en las que no valen todos los
símbolos y que sólo es verdad lo que sucede cada trescientas noches. Otra, que mientras
dormimos aquí, estamos despiertos en otro lado y que así cada hombre es dos hombres.
