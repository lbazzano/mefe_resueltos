import matplotlib.pyplot as plt
import numpy as np
from nltk import word_tokenize, sent_tokenize
import nltk
import random

nltk.download('punkt')
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.corpus import movie_reviews

# fic = open('./text_es.txt', "r")
fic = open('./text_al.txt', "r")

lines = []
for line in fic:
    lines.append(line)
fic.close()
print(line)
N=len(lines)

longitudes = []
for j in range(N):
	longitud = [len(w)for w in word_tokenize(lines[j]) if w.isalpha()]
	for i in longitud:
		longitudes.append(i)
        
figure, axes = plt.subplots(2,figsize=(8,7))

bins = np.arange(0,20,1)
h,b = np.histogram(longitudes,bins,density=True)
error1 = np.sqrt(h/len(longitudes))
axes[0].bar(b[:-1],height=h,width =1,color='pink',edgecolor='purple',yerr=error1,capsize=4)
axes[0].set_title('Histograma de ocurrencias [u.a.]')
axes[0].set_xlabel('Longitud')
axes[0].set_ylabel('Probabilidad')
axes[0].set_xticks(bins)

figure.tight_layout()

maximos=[]
for i in range(1000):
	temp=[]
	for j in range(1):
		temp.append(random.choice(longitudes))
	maximos.append(max(temp))

bins = np.arange(0,25,1)
h,b = np.histogram(maximos,bins,density=True)
error2 = np.sqrt(h/len(maximos))
axes[1].bar(b[:-1],height=h,width =1,color='pink',edgecolor='purple',yerr=error2,capsize=4)
axes[1].set_title('Distribución del estadístico t')
axes[1].set_xlabel('Longitud')
axes[1].set_ylabel('Probabilidad')
axes[1].set_xticks(bins)
plt.show()

# figure.savefig('pdf_espanol.png', dpi=300)
# figure.savefig('pdf_aleman.png', dpi=300)
