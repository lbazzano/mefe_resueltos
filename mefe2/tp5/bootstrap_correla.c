// Bootstrap para la correlación

void correlacion(vector<double> xdata, vector<double> ydata, double & corr_xy, double & ucorr_xy){

	double xsum = std::accumulate(xdata.begin(), xdata.end(), 0.0);
	double xmean = xsum / xdata.size();
	//cout<<"x mean: "<<xmean<<endl;

	double ysum = std::accumulate(ydata.begin(), ydata.end(), 0.0);
	double ymean = ysum / ydata.size();
	//cout<<"y mean: "<<ymean<<endl;

	double numerador=0;
	for (int i = 0; i < xdata.size(); ++i) numerador+=(xdata[i]-xmean)*(ydata[i]-ymean);

	double sumax=0;
	double sumay=0;
	for (int i = 0; i < xdata.size(); ++i) sumax+=pow(xdata[i]-xmean,2);
	for (int i = 0; i < xdata.size(); ++i) sumay+=pow(ydata[i]-ymean,2);
	double denominador=pow(sumax*sumay,0.5);

	corr_xy = numerador/denominador;
	ucorr_xy = (1-pow(corr_xy,2))/pow(xdata.size()-3,0.5); // Bajo la aproximacion de que x, y es normal bivariada

}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

void bootstrap_correla(){

	const int population_size=36;
	const int sample_size=36;
	int replicas=200;

	std::ifstream file;
	file.open ("fotones36.txt");
	std::vector <int> v;
	int i;
	while (file >> i) v.push_back (i);
	file.close ();
	int fot_rep[36*2];
	for (int j = 0; j < 36; ++j) fot_rep[j]    = v[2*j+1];
	for (int j = 0; j < 36; ++j) fot_rep[j+36] = v[2*j+1];

	cout.precision(2);      // Print using four decimals precision
        cout<<fixed;

	TRandom3 X_Random_variable(0); 	
	vector<double> bootstrap_sample; // acá voy a guardar todas las corr para el cálculo del error (largo 36*sample_size)
	/*
	for (int i = 0; i < replicas; ++i) {
		// voy a ver una primera réplica y voy a hacer todas las rotaciones
		for (int shift = 0; shift < 36; ++shift){
			// roto un poco la muestra
			int fot1[36];
			int fot2[36];
			for (int j = 0; j < 36; ++j){
				fot1[j] = fot_rep[j];
				fot2[j] = fot_rep[j+shift];
			}

			vector<double> b_xsample;
			vector<double> b_ysample;
			for (int j = 0; j < sample_size; ++j) {
				int indice=X_Random_variable.Uniform(1,sample_size);
				b_xsample.push_back(fot1[indice]);
				b_ysample.push_back(fot2[indice]);
				//cout <<"fot1:"<< fot1[indice] << "\tfot2: ";
				//cout << fot2[indice]<< endl;
			}
			double corr_xy_replica;
			double ucorr_xy_replica;
			correlacion(b_xsample, b_ysample, corr_xy_replica, ucorr_xy_replica);
			double statistic = corr_xy_replica;
			bootstrap_sample.push_back(statistic);
		}

	}
	*/
	for (int i = 0; i < replicas; ++i) {
		// arranco sin rotar
		int fot1[36];
		int fot2[36];
		int copy[36];
		for (int j = 0; j < 36; ++j){
			fot1[j] = fot_rep[j];
			fot2[j] = fot_rep[j];
			copy[j] = fot_rep[j];
		}

		cout << endl;
		cout <<"//////////////////////////////////////////////////////////////////////////////"<< endl;
		cout << "original: " << i << endl;
		for (int j = 0; j < sample_size; ++j) cout << fot1[j]<<" ";
		cout << endl;
		for (int j = 0; j < sample_size; ++j) cout << fot2[j] << " ";
		cout << endl;

		// voy a ver una primera réplica y voy a hacer todas las rotaciones manteniendo esa réplica
		vector<double> b_xsample;
		vector<double> b_ysample;
		vector<double> b_ycopy;
		for (int j = 0; j < sample_size; ++j) {
			int indice=X_Random_variable.Uniform(1,sample_size);
			b_xsample.push_back(fot1[indice]);
			b_ysample.push_back(fot2[indice]);
			b_ycopy.push_back(fot2[indice]);
		}

		cout << "replica: " << i << endl;
		for (auto i: b_xsample	) cout << i << ' ';
		cout << endl;
		for (auto i: b_xsample	) cout << i << ' ';
		cout << endl;

		for (int shift = 0; shift < 36; ++shift){
			// roto un poco la muestra
			/*
			for (int j = 0; j < 36-shift; ++j){
				cout << fot2[j]<< endl;
				fot2[j] = copy[j+shift];
				cout << fot2[j]<< endl;
			}
			for (int j = 36-shift; j < 36; ++j){
				cout << fot2[j]<< endl;
				fot2[j] = copy[j-36+shift];
				cout << fot2[j]<< endl;
			}
			*/
			for (int j = 0; j < 36-shift; ++j){
				b_ysample[j] = b_ycopy[j+shift];
			}
			for (int j = 36-shift; j < 36; ++j){
				b_ysample[j] = b_ycopy[j-36+shift];
			}
			//cout << endl;
			/*
			vector<double> b_xsample;
			vector<double> b_ysample;
			for (int j = 0; j < sample_size; ++j) {
				//int indice=X_Random_variable.Uniform(1,sample_size);
				//b_xsample.push_back(fot1[indice]);
				//b_ysample.push_back(fot2[indice]);
				b_xsample.push_back(fot1[j]);
				b_ysample.push_back(fot2[j]);
				//cout <<"fot1: "<< fot1[j] << "\tfot2: " << fot2[j]<< endl;
			}
			*/
			cout <<"////////////////////////"<< endl;
			cout << "rotación: " << shift << endl;
			for (auto i: b_xsample	) cout << i << ' ';
			cout << endl;
			for (auto i: b_ysample	) cout << i << ' ';
			cout << endl;

			double corr_xy_replica;
			double ucorr_xy_replica;
			correlacion(b_xsample, b_ysample, corr_xy_replica, ucorr_xy_replica);
			double statistic = corr_xy_replica;
			bootstrap_sample.push_back(statistic);
		}
		cout << endl;

	}


	double corr[36];
	double tita[36];
	double corr_error[36];
	double tita_error[36];

	for (int shift = 0; shift < 36; ++shift){
		// calculo el error de la correlación
		double mean = 0;
		for (int i = 0; i < replicas; ++i) {
			mean += bootstrap_sample[36*i+shift] / replicas;
		}
		double var = 0;
		for (int i = 0; i < replicas; ++i) {
			var += pow(bootstrap_sample[36*i+shift] - mean, 2) / (replicas-1.);
		}
		corr_error[shift] = sqrt(var);

		// calculo la correlación
		vector<double> fot1;  
		vector<double> fot2;  
		for (int j = 0; j < 36; ++j){
			fot1.push_back(fot_rep[j]);
			fot2.push_back(fot_rep[j+shift]);
		}
		double corr_xy;
		double ucorr_xy;
		correlacion(fot1, fot2, corr_xy, ucorr_xy);
		corr[shift] = corr_xy;
		corr[shift] = abs(corr_xy);
		//corr_error[shift] = ucorr_xy;

		// calculo tita
		tita[shift] = (360/36)*shift;
		tita_error[shift] = 0;

		// sigmas del cero
		double sigm_zero = abs(corr[shift]/corr_error[shift]);
		double sigm_one = abs((corr[shift]-1)/corr_error[shift]);
		
		cout << "\tcorrelacion: " << corr[shift] <<"  \terror: "<< corr_error[shift] << "\ttita: " << tita[shift]  << "\tsigmas a 0: " << sigm_zero << "\tsigmas a 1: " << sigm_one<< endl;

	}

	auto c1 = new TCanvas("c1","A Simple Graph with error bars",200,10,700,500);
	auto gr = new TGraphErrors(36,tita,corr,tita_error,corr_error);
	gr->SetTitle("Autocorrelacion en funcion del angulo rotado");
	TLine *line = new TLine(-15,0,365,0);
	gr->SetMarkerColor(kOrange+1);
	line->SetLineWidth(2);
	line->SetLineStyle(2);
	gr->GetXaxis()->SetLimits(-15,365);
	gr->SetMarkerStyle(20);
	gPad->SetGrid();
	gr->Draw("ALP");
	line->Draw("ALP");
}


/*
   auto * correlacion_gr = new TGraph();
   for (int shift = 0; shift < 36; ++shift){


   int fot1[36];
   int fot2[36];
   double tita[36];

   for (int j = 0; j < 36; ++j){
   fot1[j] = fot_rep[j];
   fot2[j] = fot_rep[j+shift];
   tita[j] = j*360/36;
   }

   for (int j = 0; j < 36; ++j){
   cout << fot1[j] << " " << fot2[j] << " " << tita[j] << endl;
   }


   vector<double> xpoblacion;  
   vector<double> ypoblacion;  
   vector<double> xsample;  
   vector<double> ysample;  
   vector<double> bootstrap_sample; 

//////////////////////////////////////////////////////////////////////////////////////////////////// correlacion de los datos (puntos a los que quiero calcular el error)
TRandom3 X_Random_variable(0); 

for (int i = 0; i < population_size; ++i) {
//xpoblacion.push_back(tiempo[i]);
//ypoblacion.push_back(distancia[i]);
xpoblacion.push_back(fot1[i]);
ypoblacion.push_back(fot2[i]);
}

double corr_xy;
double ucorr_xy;
correlacion(xpoblacion, ypoblacion, corr_xy, ucorr_xy);
cout<<"Correlacion de la poblacion: "<<corr_xy<<"  +/- "<<ucorr_xy<<endl;


//////////////////////////////////////////////////////////////////////////////////////////////////// Tomo la muestra sin reposicion
for (int i = 0; i < sample_size; ++i) {
xsample.push_back(xpoblacion[i]);//[indice[i]]);
ysample.push_back(ypoblacion[i]);//[indice[i]]);
}

TGraph * gr_sample= new TGraph(xsample.size(), &xsample[0], &ysample[0]);
gr_sample->SetTitle("Muestra");
gr_sample->GetXaxis()->SetTitle("Tiempo (minutos)");
gr_sample->GetYaxis()->SetTitle("Distancia (km)"); 
gr_sample->SetMarkerStyle(21);

//gr_sample->Draw("AP");

double corr_xy_sample;
double ucorr_xy_sample;
correlacion(xsample, ysample, corr_xy_sample, ucorr_xy_sample);
cout<<"Correlacion de la muestra: "<<corr_xy_sample<<"  +/- "<<ucorr_xy_sample<<endl;

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

TH1D * histo_bootstrap  =  new TH1D("histo_bootstrap","", 1000, 0, 1);
//histo_bootstrap -> Sumw2();

//canvas->cd(3);
//canvas->cd(3)->SetGridx();
//canvas->cd(3)->SetGridy();

vector<double> b_xsample;  
vector<double> b_ysample; 
double corr_xy_replica;
double ucorr_xy_replica;

for (int i = 0; i < replicas; ++i) {
	double b_sample;
	double b_sample_suma;
	for (int j = 0; j < sample_size; ++j) {
		int indice=X_Random_variable.Uniform(1,sample_size);
		b_xsample.push_back(xsample[indice]);
		b_ysample.push_back(ysample[indice]);	
	}

	correlacion(b_xsample, b_ysample, corr_xy_replica, ucorr_xy_replica);

	double statistic=corr_xy_replica;
	bootstrap_sample.push_back(statistic);
	histo_bootstrap->Fill(statistic);
}

double ucorr_xy_bootstrap;
double sum = std::accumulate(bootstrap_sample.begin(), bootstrap_sample.end(), 0.0);
double mean = sum / replicas;

double var=0;
for (int i = 0; i < replicas; ++i) {
	var+=pow(bootstrap_sample[i]-mean,2)/(replicas-1);
}
ucorr_xy_bootstrap=pow(var,0.5);

histo_bootstrap->SetTitle("Histograma del rho de las replicas");
histo_bootstrap->GetXaxis()->SetTitle("correlacion de la replica");
histo_bootstrap->GetYaxis()->SetTitle("#");
histo_bootstrap->SetLineWidth(2);

//histo_bootstrap->Draw("HIST");

double ucorr_xy_bootstrap2=histo_bootstrap->GetStdDev();
cout<<"Incerteza en la correlación obtenida por bootstrap: "<<ucorr_xy_bootstrap<<endl;
cout<<"Incerteza en la correlación obtenida por bootstrap del histograma: "<<ucorr_xy_bootstrap2<<endl;

correlacion_gr->SetPoint(shift,tita[shift],corr_xy);
}
correlacion_gr->Draw("APL");
*/

