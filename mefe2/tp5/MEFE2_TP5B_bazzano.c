// Bootstrap para la correlación

void correlacion(vector<double> xdata, vector<double> ydata, double & corr_xy, double & ucorr_xy){

	double xsum = std::accumulate(xdata.begin(), xdata.end(), 0.0);
	double xmean = xsum / xdata.size();
	//cout<<"x mean: "<<xmean<<endl;

	double ysum = std::accumulate(ydata.begin(), ydata.end(), 0.0);
	double ymean = ysum / ydata.size();
	//cout<<"y mean: "<<ymean<<endl;

	double numerador=0;
	for (int i = 0; i < xdata.size(); ++i) numerador+=(xdata[i]-xmean)*(ydata[i]-ymean);

	double sumax=0;
	double sumay=0;
	for (int i = 0; i < xdata.size(); ++i) sumax+=pow(xdata[i]-xmean,2);
	for (int i = 0; i < xdata.size(); ++i) sumay+=pow(ydata[i]-ymean,2);
	double denominador=pow(sumax*sumay,0.5);

	corr_xy = numerador/denominador;
	ucorr_xy = (1-pow(corr_xy,2))/pow(xdata.size()-3,0.5); // Bajo la aproximacion de que x, y es normal bivariada

}

////////////////////////////////////////////////////////////////////////////////////////////////////

void MEFE2_TP5B_bazzano(){

	const int population_size=36;
	const int sample_size=36;
	int replicas=20;

	std::ifstream file;
	file.open ("fotones36.txt");
	std::vector <int> v;
	int i;
	while (file >> i) v.push_back (i);
	file.close ();
	int fot_rep[36*2];
	for (int j = 0; j < 36; ++j) fot_rep[j]    = v[2*j+1];
	for (int j = 0; j < 36; ++j) fot_rep[j+36] = v[2*j+1];

	cout.precision(2);      // Print using four decimals precision
        cout<<fixed;

	TRandom3 X_Random_variable(0); 	
	vector<double> bootstrap_sample; // acá voy a guardar todas las corr para el cálculo del error (largo 36*sample_size)

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Bootstrap
	for (int i = 0; i < replicas; ++i) {
		// arranco sin rotar
		int fot1[36];
		int fot2[36];
		int copy[36];
		for (int j = 0; j < 36; ++j){
			fot1[j] = fot_rep[j];
			fot2[j] = fot_rep[j];
			copy[j] = fot_rep[j];
		}

		// voy a ver una primera réplica y voy a hacer todas las rotaciones manteniendo esa réplica
		vector<double> b_xsample;
		vector<double> b_ysample;
		vector<double> b_ycopy;
		for (int j = 0; j < sample_size; ++j) {
			int indice=X_Random_variable.Uniform(1,sample_size);
			b_xsample.push_back(fot1[indice]);
			b_ysample.push_back(fot2[indice]);
			b_ycopy.push_back(fot2[indice]);
		}

		for (int shift = 0; shift < 36; ++shift){
			// roto un poco el segundo vector de la replica
			for (int j = 0; j < 36-shift; ++j) b_ysample[j] = b_ycopy[j+shift];
			for (int j = 36-shift; j < 36; ++j)b_ysample[j] = b_ycopy[j-36+shift];
			double corr_xy_replica;
			double ucorr_xy_replica;
			correlacion(b_xsample, b_ysample, corr_xy_replica, ucorr_xy_replica);
			double statistic = corr_xy_replica;
			bootstrap_sample.push_back(statistic);
		}

	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// Guardo lo que me interesa
	double corr[36];
	double tita[36];
	double corr_error[36];
	double tita_error[36];

	for (int shift = 0; shift < 36; ++shift){
		// calculo el error de la correlación
		double mean = 0;
		for (int i = 0; i < replicas; ++i) mean += bootstrap_sample[36*i+shift] / replicas;

		double var = 0;
		for (int i = 0; i < replicas; ++i) var += pow(bootstrap_sample[36*i+shift] - mean, 2) / (replicas-1.);
		corr_error[shift] = sqrt(var);

		// calculo la correlación
		vector<double> fot1;  
		vector<double> fot2;  
		for (int j = 0; j < 36; ++j){
			fot1.push_back(fot_rep[j]);
			fot2.push_back(fot_rep[j+shift]);
		}
		double corr_xy;
		double ucorr_xy;
		correlacion(fot1, fot2, corr_xy, ucorr_xy);
		corr[shift] = corr_xy;
		//corr[shift] = abs(corr_xy);
		//corr_error[shift] = ucorr_xy;

		// calculo tita
		tita[shift] = (360/36)*shift;
		tita_error[shift] = 0;

		// sigmas del cero
		double sigm_zero = abs(corr[shift]/corr_error[shift]);
		double sigm_one = abs((corr[shift]-1)/corr_error[shift]);
		
		cout << "\tcorrelacion: " << corr[shift] <<"  \terror: "<< corr_error[shift] << "\ttita: " << tita[shift]  << "\tsigmas a 0: " << sigm_zero << "\tsigmas a 1: " << sigm_one<< endl;

	}

	auto c1 = new TCanvas("c1","A Simple Graph with error bars",200,10,700,500);
	auto gr = new TGraphErrors(36,tita,corr,tita_error,corr_error);
	gr->SetTitle("Autocorrelacion en funcion del angulo rotado");
	TLine *line = new TLine(-15,0,365,0);
	gr->SetMarkerColor(kOrange+1);
	line->SetLineWidth(2);
	line->SetLineStyle(2);
	gr->GetXaxis()->SetLimits(-15,365);
	gr->SetMarkerStyle(20);
	gPad->SetGrid();
	gr->Draw("ALP");
	line->Draw("ALP");
}
