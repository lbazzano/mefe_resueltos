// Este Scripts compara la performance de Bootstrap contra Toys Montecarlo
// Ayuda a visualizar que tamaño de muestra y número de réplicas son suficientes
// para obtener una estimación razonable de la incerteza en el promedio de una muestra Poissoniana

void bootstrap_vs_toy(){
	int Ntot = 3;					// cantidad de tamaños de muestras que voy a usar
	int sample_size_vec[3]={100,1000,10000} ; 	// voy a ver tres curvas (tres tamaños de muestra distintos)
	char  sample_size_vec_leg[3][10]={"100","1000","10000"} ; 	// voy a ver tres curvas (tres tamaños de muestra distintos)
	int maxReplicas = 100;				// máximo de replicas
	int ptsReplicas = 100;				// cantidad de puntos en el eje x (replicas)


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////      CAMBIAR LO DE ARRIBA NADA MÁS           /////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	int salto = maxReplicas/ptsReplicas;

	auto c2 = new TCanvas("c2","c2",1000,700);
	TGraph *g[3];
	auto mg = new TMultiGraph();
	auto legend = new TLegend(0.6,0.1,0.9,0.3);
	legend->SetHeader("Tamano de la muestra",""); // option "C" allows to center the header


	for (int ss = 0; ss < Ntot; ++ss) {
		int sample_size = sample_size_vec[ss];
		g[ss] = new TGraph();
		int rr = 0;

		for (int replicas = 0; replicas < maxReplicas; replicas=replicas+salto) {
			cout << replicas <<" \t replicas de un total de " << maxReplicas <<"\t (tamaño de una muestra = "<< sample_size <<")" <<endl;

			double t_sample;
			double t_sample_suma;
			double mu=50;
			double sigma = sqrt(50);
			double sigmam = sigma/sqrt(sample_size);
			int emin=mu-3*sigma;
			int emax=mu+3*sigma;
			int nsigmas=10;

			int bines = (emax-emin);

			vector<double> population;  
			vector<double> sample;  
			vector<double> bootstrap_sample; 

			TRandom3 X_Random_variable(0); 

			////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////

			//gROOT->SetStyle("Plain");
			//TCanvas * canvas = new TCanvas("canvas","canvas",2000,1000);
			//canvas->Divide(2,2);

			////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////

			//canvas->cd(1);
			//canvas->cd(1)->SetGridx();
			//canvas->cd(1)->SetGridy();

			//TF1 *pdf = new TF1("pdf","TMath::Poisson(x,50)",emin,emax);
			//pdf->SetLineColor(kRed);
			//pdf->Draw();


			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Aca empiezo los Toys

			//TH1D * histo_toy  =  new TH1D("histo_toy","", bines, mu-nsigmas*sigmam, mu+nsigmas*sigmam);
			//histo_toy -> Sumw2();

			//canvas->cd(2);
			//canvas->cd(2)->SetGridx();
			//canvas->cd(2)->SetGridy();

			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Hago los toys   

			//double toys_mean = 0;
			double toys_mediana_vec[replicas];
			for (int i = 1; i < replicas+1; ++i) {
				double t_sample = 0;
				//double t_sample_suma = 0;
				double toys_random_vec[sample_size];
				for (int j = 1; j < sample_size+1; ++j) {
					t_sample = X_Random_variable.Poisson(mu);
					toys_random_vec[i-1] = t_sample;
					//t_sample_suma+= t_sample;
				}
				//double mean = double(t_sample_suma)/sample_size;

				sort(toys_random_vec, toys_random_vec + sample_size);
				toys_mediana_vec[i-1] = toys_random_vec[sample_size/2];
				//t_sample_suma=0;
				//double statistic=mean;
				//toys_mean+=mean;
				//histo_toy->Fill(statistic);
			}


			//toys_mean=toys_mean/replicas;

			/*
			cout<<"El promedio de los toys es:   "<<toys_mean<<endl;
			histo_toy->SetTitle("Toys del promedio");
			histo_toy->GetXaxis()->SetTitle("promedio de la replica");
			histo_toy->GetYaxis()->SetTitle("#");
			histo_toy->SetLineWidth(2);

			histo_toy->SetLineColor(kRed);
			histo_toy->Draw("E0 HIST");
			*/

			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Aca empiezo el bootstrap

			////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////

			//TH1D * histo_sample  =  new TH1D("histo_sample","", bines, emin, emax);
			//histo_sample -> Sumw2();

			//canvas->cd(3);
			//canvas->cd(3)->SetGridx();
			//canvas->cd(3)->SetGridy();


			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Tomo la muestra 

			//double x_sample_mean = 0;
			double x_random_vec[sample_size];
			for (int i = 1; i < sample_size+1; ++i) {
				double x_sample = 0;
				//x_sample=X_Random_variable.Uniform(emin,emax);
				x_sample=X_Random_variable.Poisson(mu);
				//x_sample_mean+=x_sample;
				x_random_vec[i-1] = x_sample;
				sample.push_back(x_sample);
				//histo_sample->Fill(x_sample);
			}

			sort(x_random_vec, x_random_vec + sample_size);
			double x_sample_mediana = x_random_vec[sample_size/2];

			//x_sample_mean=double(x_sample_mean)/sample_size;
			/*
			cout<<"El promedio de la muestra es: "<<x_sample_mean<<endl;
			histo_sample->SetTitle("Muestra");
			histo_sample->GetXaxis()->SetTitle("realizacion");
			histo_sample->GetYaxis()->SetTitle("#");
			histo_sample->SetLineWidth(2);

			histo_sample->SetLineColor(kBlue);
			//histo_sample->Draw("E0 HIST");
			*/
			////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////

			//TH1D * histo_bootstrap  =  new TH1D("histo_bootstrap","", bines, mu-nsigmas*sigmam, mu+nsigmas*sigmam);
			//histo_bootstrap -> Sumw2();

			//canvas->cd(4);
			//canvas->cd(4)->SetGridx();
			//canvas->cd(4)->SetGridy();

			////////////////////////////////////////////////////////////////////////////////////////////////////
			// Hago las replicas, estas son con reposicion
			//
			//
			//
			//
			double bootstrap_mean = 0;
			double b_mediana_vec[replicas] ;
			double b_sample_vec[replicas];
			for (int i = 1; i < replicas+1; ++i) {
				double b_sample=0;
				//double b_sample_suma=0;
				double b_random_vec[sample_size];
				for (int j = 1; j < sample_size+1; ++j) {
					int indice=X_Random_variable.Uniform(1,sample_size);
					b_sample=sample[indice];
					b_random_vec[j-1] = b_sample;
					//b_sample_suma+=b_sample; // sumé "sample_size" valores de la variable proveniente del histograma de la muestra
				}
				//double mean  = b_sample_suma/sample_size;// mean de esos valores, o sea el estadístico tita(b)
				
                                sort(b_random_vec, b_random_vec + sample_size);
                                b_mediana_vec[i-1] = b_random_vec[sample_size/2];
				//b_sample_vec[i-1] = mean ;// voy a acumular en este vector los tita(b)
				//b_sample_suma=0;
				//double statistic=mean;
				//bootstrap_mean+=mean/replicas; // acá estoy calculando el tita(.)  promedio de estadísticos
				bootstrap_mean+=b_mediana_vec[i-1]/replicas; // acá estoy calculando el tita(.)  promedio de estadísticos
				//cout<<"Estadistico: "<<statistic<<endl;
				//bootstrap_sample.push_back(statistic);
				//histo_bootstrap->Fill(statistic);
			}

			double bootstrap_var = 0;
			for (int i = 1; i < replicas+1; ++i) {
				//double b_sample = b_sample_vec[i-1];
				double b_sample = b_mediana_vec[i-1];
				bootstrap_var  += pow(b_sample - bootstrap_mean, 2) / (replicas - 1);// mean
			}

			
			 /*
			cout<<"El promedio de las replicas es:   "<< bootstrap_mean      <<endl;
			cout<<"El error    de las replicas es:   "<< sqrt(bootstrap_var) <<endl;
			//double bias=abs(bootstrap_mean-x_sample_mean);
			double bias=abs(bootstrap_mean-x_sample_mediana);
			cout<<"por lo tanto el bias es:          "<<bias<<endl;
			histo_bootstrap->SetTitle("Bootstrap para el promedio");
			histo_bootstrap->GetXaxis()->SetTitle("promedio de la replica");
			histo_bootstrap->GetYaxis()->SetTitle("#");
			histo_bootstrap->SetLineWidth(2);

			histo_bootstrap->SetLineColor(kBlue);
			//histo_bootstrap->Draw("E0 HIST");
			*/
			
			g[ss]->SetPoint(rr,replicas,sqrt(bootstrap_var));
			rr++;
			//histo_bootstrap->Reset();
			//histo_toy->Reset();
			//histo_sample->Reset();

		}
		g[ss]->SetMarkerStyle(20+ss);
		g[ss]->SetLineColor(ss+2);
		g[ss]->SetMarkerColor(ss+2);
		char * leg = sample_size_vec_leg[ss];
		legend->AddEntry(g[ss],leg,"L");
		mg->Add(g[ss]);
		
	}

	mg->Draw("AL");
	mg->GetXaxis()->SetTitle("Cantidad de replicas");
	mg->GetYaxis()->SetTitle("Incerteza");
	legend->Draw("");
	c2->SetGrid();

	// Change the axis limits
	//gPad->Modified();
	//mg->GetXaxis()->SetLimits(1.5,7.5);
	//mg->SetMinimum(0.);
	//mg->SetMaximum(10.);

}
