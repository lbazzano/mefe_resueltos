// Este Scripts compara la performance de Bootstrap contra Toys Montecarlo
// Ayuda a visualizar que tamaño de muestra y número de réplicas son suficientes
// para obtener una estimación razonable de la incerteza en el promedio de una muestra Poissoniana

void MEFE2_TP5A_bazzano(){
	int Ntot = 3;					// cantidad de tamaños de muestras que voy a usar
	int sample_size_vec[3]={10,100,1000} ; 	// voy a ver tres curvas (tres tamaños de muestra distintos)
	char  sample_size_vec_leg[3][10]={"10","100","1000"} ; 	// voy a ver tres curvas (tres tamaños de muestra distintos)
	int maxReplicas = 100;				// máximo de replicas
	int ptsReplicas = 100;				// cantidad de puntos en el eje x (replicas)
	//int maxReplicas = 100000;				// máximo de replicas
	//int ptsReplicas = 6;				// cantidad de puntos en el eje x (replicas)
	int replicas_vec[1000];
	for (int rep = 0; rep < ptsReplicas; rep=rep+1) {
		//replicas_vec[rep] = pow(10,rep)+1;
		replicas_vec[rep] = rep;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////      CAMBIAR LO DE ARRIBA NADA MÁS           /////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	int salto = maxReplicas/ptsReplicas;

	auto c2 = new TCanvas("c2","c2",1000,700);
	TGraph *g[3];
	auto mg = new TMultiGraph();
	auto legend = new TLegend(0.7,0.8,1,1);
	legend->SetHeader("Tamano de la muestra",""); // option "C" allows to center the header


	for (int ss = 0; ss < Ntot; ++ss) {
		int sample_size = sample_size_vec[ss];
		g[ss] = new TGraph();
		int rr = 0;

		//for (int replicas = 0; replicas < maxReplicas; replicas=replicas+salto) {
		for (int rep = 0; rep < ptsReplicas; rep=rep+1) {
			int replicas = replicas_vec[rep];
			cout << replicas <<" \t replicas de un total de " << maxReplicas <<"\t (tamaño de una muestra = "<< sample_size <<")" <<endl;

			double mu=50;

			vector<double> population;  
			vector<double> sample;  
			vector<double> bootstrap_sample; 

			TRandom3 X_Random_variable(0); 

			double toys_mediana_vec[replicas];
			for (int i = 1; i < replicas+1; ++i) {
				double t_sample = 0;
				double toys_random_vec[sample_size];
				for (int j = 1; j < sample_size+1; ++j) {
					//t_sample = X_Random_variable.Poisson(mu);
					t_sample = X_Random_variable.Uniform(0,mu);
					toys_random_vec[i-1] = t_sample;
				}
				sort(toys_random_vec, toys_random_vec + sample_size);
				toys_mediana_vec[i-1] = toys_random_vec[sample_size/2];
			}

			double x_random_vec[sample_size];
			for (int i = 1; i < sample_size+1; ++i) {
				double x_sample = 0;
				//x_sample = X_Random_variable.Poisson(mu);
				x_sample = X_Random_variable.Uniform(0,mu);
				x_random_vec[i-1] = x_sample;
				sample.push_back(x_sample);
				//histo_sample->Fill(x_sample);
			}

			sort(x_random_vec, x_random_vec + sample_size);
			double x_sample_mediana = x_random_vec[sample_size/2];

			double bootstrap_mean = 0;
			double b_mediana_vec[replicas];
			double b_sample_vec[replicas];
			for (int i = 1; i < replicas+1; ++i) {
				double b_sample=0;
				//double b_sample_suma=0;
				double b_random_vec[sample_size];
				for (int j = 1; j < sample_size+1; ++j) {
					int indice=X_Random_variable.Uniform(1,sample_size);
					b_sample=sample[indice];
					b_random_vec[j-1] = b_sample;
					//b_sample_suma+=b_sample; // sumé "sample_size" valores de la variable proveniente del histograma de la muestra
				}
				//double mean  = b_sample_suma/sample_size;// mean de esos valores, o sea el estadístico tita(b)
				
                                sort(b_random_vec, b_random_vec + sample_size);
                                b_mediana_vec[i-1] = b_random_vec[sample_size/2];
				bootstrap_mean+=b_mediana_vec[i-1]/replicas; // acá estoy calculando el tita(.)  promedio de estadísticos
			}

			double bootstrap_var = 0;
			for (int i = 1; i < replicas+1; ++i) {
				//double b_sample = b_sample_vec[i-1];
				double b_sample = b_mediana_vec[i-1];
				bootstrap_var  += pow(b_sample - bootstrap_mean, 2) / (replicas - 1);// mean
			}

			g[ss]->SetPoint(rr,replicas,sqrt(bootstrap_var));
			//cout << rr << "  "<< replicas << "  "<<sqrt(bootstrap_var)<<endl;
			rr++;

		}
		g[ss]->SetMarkerStyle(20+ss);
		g[ss]->SetLineColor(ss+2);
		g[ss]->SetMarkerColor(ss+2);
		char * leg = sample_size_vec_leg[ss];
		legend->AddEntry(g[ss],leg,"L");
		mg->Add(g[ss]);
		
	}

	mg->Draw("ALP");
	mg->GetXaxis()->SetTitle("Cantidad de replicas");
	//gPad->SetLogx();
	mg->GetYaxis()->SetTitle("Incerteza");
	legend->Draw("");
	c2->SetGrid();

	// Change the axis limits
	//gPad->Modified();
	//mg->GetXaxis()->SetLimits(1,maxReplicas*10);
	//mg->SetMinimum(0.);
	//mg->SetMaximum(10.);

}
