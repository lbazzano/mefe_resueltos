// Herramienta para armar intervalos de confianza frecuentistas -exactos- usando MC

// PARTE A
// 1) Genero n valores de la V.A. x con distribución exponencial que depende de tau. Calculo el valor de t, un estimador de tau (o de una función de tau), en este caso el promedio de los x.
// 2) Repito 1 N veces para construir la distribución de t.
// 3) Encuentro tmin y tmax entre los cuales integro un CL con algún criterio elegido.
// 4) Repito 1,2 y 3 para 100 valores distintos de tau entre 0 y 10 para armar el cinturon de confianza tau vs t.
//
// Parte B
// Repito PARTE A para:
// 1) La mediana de los x.
// 2) t = sum (xi+xi**2+xi**3+xi**4)
//
// PARTE C
// Repito PARTE A para la q de wilks: q=-2log(L/Lmax).
//
// PARTE D
// Utilizo Wilks para calcular el intervalo de confianza para el parámetro tau de la  exponencial con un dado CL.

void MEFE2_TP2_bazzano(	double tau=4,		// tau real con el cual simulo los datos (creo que para el caso de LL ? No lo uso para nada).
	       		int n=100,		// cantidad de muestras de la variable aleatoria x.
			int N=100000,		// cantidad de veces que calculo la variable t para armar su distribución (mejor dicho, la cantidad de explonenciales).
		       	double CL = 0.683,	// nivel de confianza que quiero ver 0.955 0.997.
			int Ntaus=100,		// cantidad de taus /nresolución en el eje y del cinturón.

		       	double tauMin=0,	// tau mínimo.
		       	double tauMax=10,	// tau máximo.
     			double tau_obs=5.56){	// tau medido ?

	TRandom3 r(0);  // Initialize random generator
	double x[n];    // Will store the n random variables

	double nsigmas = ROOT::Math::gaussian_quantile((1+CL)/2,1);
	double delta = nsigmas*nsigmas/2;

	
	double tMax = tauMax*10;
	auto* h_t = new TH1F("h_t","t Distribution",10000,0,tMax);

	//double tMax = 100000000;
	//auto* h_t = new TH1F("h_t","t Distribution",10000,0,100000000); // para el estimador raro

	// PARTE A (4)
	double taus[Ntaus];
	double tL[Ntaus];    // Will store the n random variables
	double tU[Ntaus];    // Will store the n random variables
	
	double cob_p = 0;
	for (int jtau = 0; jtau < Ntaus; ++jtau) {
		taus[jtau] = tauMin + jtau * (tauMax-tauMin) / (Ntaus-1);

		// PARTE A (2)
		for (int iexp = 0; iexp < N; ++iexp) {

			// PARTE A (1)
			for (int i = 0; i < n; ++i) x[i] = r.Exp(taus[jtau]);

			
			// Si quiero ver algunas de las distribuciones exponenciales descomento esto://////////////
			// Configuración recomendada para visualizar esto lindo:	n=10000 N=100 Ntaus=1000
			/*
			if (iexp == N/2){
				auto X = new TH1D("","",1000,0,tMax);
				for (int i = 0; i < n; ++i){
					X->Fill(x[i]);
				}
				X->Draw("E");
				X->GetXaxis()->SetRangeUser(0,tauMax);
				X->GetYaxis()->SetRangeUser(0,1000);
				X->SetTitle("Distribucion de la variable aleatoria x;x;Entradas;");
				gPad->Update();
				gPad->WaitPrimitive();
				cout << taus[jtau];
				X->Reset();
			}
			///////////////////////////////////////////////////////////////////////////////////////////
			*/

			// Estimadores:
			
			
			// Promedio:	
			double x_sum = 0;
			for (auto x_i : x) x_sum += x_i;
			double t = x_sum/n;
			

			// Parte B (1)
			// Mediana: 
			//sort(x, x + n);
			//double t =( x[(n-1)/2]+x[n/2])/2;
			//cout << t << endl;

			// Parte B (2)
			// Función rara:
			//double x_rara = 0;
			//for (auto x_i : x)  x_rara += x_i + pow(x_i,2) + pow(x_i,3) + pow(x_i,4);
			//double t = x_rara;
			//cout << t << endl;
			
			// Parte C	
			/*
			double x_sum = 0;
			for (auto x_i : x) x_sum += x_i;
			double t_hat = x_sum/n;
			double L     = 0;
			double L_hat = 0;
			for (auto x_i : x)  L     += log(  (1./taus[jtau]) * exp(-x_i/taus[jtau])  );
			for (auto x_i : x)  L_hat += log(  (1./t_hat)      * exp(-x_i/t_hat     )  ); // mejor estimador?	
			double t = -2 * (L-L_hat);	

			// Parte D	
			auto logL =      new TF1("f","log(pow(x,[0])*exp([1]/x))");//"pow(x,-[0])*exp(-[1]/x)");
			logL->SetParameter(0,-n);
			logL->SetParameter(1,-x_sum);
			double Lmax = logL->Eval(t_hat);
			double xmin = logL->GetX(Lmax-delta,0.1,t_hat);
		    	double xmax = logL->GetX(Lmax-delta,t_hat,100*t_hat);
			if(taus[jtau]<xmax && taus[jtau]>xmin){
			       	cob_p = cob_p + 1./(N*n);
			}
			*/

			h_t -> Fill(t);
		}
		// PARTE A (3)
		double xq_d[1] = {(1+CL)/2};
		double xq_i[1] = {(1-CL)/2};
		double yq_d[1];           // array donde guardarlo
		double yq_i[1];           // array donde guardarlo
		h_t->GetQuantiles(1, yq_d, xq_d); // Busco el cuantil en la distribución
		h_t->GetQuantiles(1, yq_i, xq_i); // Busco el cuantil en la distribución
		
		// Chequeo el cálculo de la integral a izquierda y a derecha de los U y L elegidos para esta distribución.
		h_t->Scale( 1./h_t->Integral());
		double I_i = h_t->Integral(h_t->FindBin(0),h_t->FindBin(yq_i[0]),""); 
		double I_d = h_t->Integral(h_t->FindBin(yq_d[0]),h_t->FindBin(tMax),""); 
		cout << "Las integrales son a izquierda: "<< I_i << " y a derecha: " << I_d ; 
		
		// Si quiero ver las distribuciones del estimador descomento esto://///////////////////////////////////////////
		
		
		// Configuración recomendada para visualizar esto lindo:	n=100 N=10000 Ntaus=1000
		//h_t->Scale( 1./h_t->Integral(),"WIDTH");
		/*
		h_t->Draw("EL");
		//auto fchi2 = new TF1("chi2","ROOT::Math::chisquared_pdf(x,1)",0,100); // para graficar la chi2(1)
		//fchi2->Draw("same");

		auto shade = (TH1F*)h_t->Clone("CL");
		shade->GetXaxis()->SetRange(h_t->FindBin(yq_i[0]),h_t->FindBin(yq_d[0]));
		shade->SetFillStyle(3335);
		shade->SetFillColor(3);
		shade->Draw("same");
		h_t->GetXaxis()->SetRangeUser(0,tauMax+5);
		h_t->SetTitle("Distribucion del estimador t;t;Entradas;");
		gPad->Update();
		gPad->WaitPrimitive();
		*/
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		h_t->Reset();			

		cout.precision(2);      // Print using four decimals precision
		cout<<fixed;            // ...exactly four decimals
		cout << " CL entre " << yq_i[0] << " y " << yq_d[0] << "\t Van " << jtau<< " de " << Ntaus<< "    -----> tau:" << taus[jtau] << endl;

		tL[jtau] = yq_i[0]; 
		tU[jtau] = yq_d[0];
	}
	cout << "cobertura promedio para item D:" << cob_p*100 << " %" << endl;
	gStyle->SetOptStat(0);
	auto c1 = new TCanvas("c1","Cinturon de confianza",900,900);
	c1->cd(1);

	TGraph* gr_U = new TGraph(Ntaus,tU,taus);
	gr_U->SetMarkerStyle(20);
	gr_U->SetMarkerSize(0.6);
	gr_U->SetMarkerColor(1);
	gr_U->SetLineColor(2);
	gr_U->SetLineWidth(3);

	TGraph* gr_L = new TGraph(Ntaus,tL,taus);
	gr_L->SetMarkerColor(1);
	gr_L->SetMarkerSize(0.6);
	gr_L->SetMarkerStyle(20);	
	gr_L->SetLineColor(2);
	gr_L->SetLineWidth(3);

	gPad->SetGrid();
	
	// cobertura (usando el tau real como si fuese el medido)
	double U_lim = gr_U->Eval(tau_obs);
	double L_lim = gr_L->Eval(tau_obs);
	double intervalo[2] = {U_lim , L_lim};
	double tau_obs_x[2] = {tau_obs, tau_obs};
	TGraph* int_obs = new TGraph(2,tau_obs_x,intervalo);
	int_obs->SetMarkerStyle(20);
	int_obs->SetMarkerSize(1.5);
	int_obs->SetMarkerColor(1);
	int_obs->SetLineColor(1);
	int_obs->SetLineWidth(5);

	double cob = 0;
	for (int exp = 0; exp<N; exp++){
		for (int i = 0; i < n; ++i) x[i] = r.Exp(tau_obs);
		
		// Promedio:	
		double x_sum = 0;
		for (auto x_i : x) x_sum += x_i;
		double t = x_sum/n;
		

		// Parte B (1)
		// Mediana: (n tiene que ser par)
		//sort(x, x + n);
		//double t =( x[(n-1)/2]+x[n/2])/4;
		//cout << t << endl;

		// Parte B (2)
		// Función rara:
		/*
		double x_rara = 0;
		for (auto x_i : x)  x_rara += x_i + pow(x_i,2) + pow(x_i,3) + pow(x_i,4);
		double t = x_rara;
		//cout << t << endl;
		*/
		
		if (t > U_lim && t < L_lim){
			cob = cob + 1;
		}
	}
	cout <<"Para tau="<< tau_obs << " (U: "<< U_lim << " / L: "<< L_lim <<") la cobertura es " << 100*cob/N << " %" << endl;

	TMultiGraph *mg = new TMultiGraph();

	mg->Add(gr_U);
	mg->Add(gr_L);
	mg->Add(int_obs);
	//mg->GetYaxis()->SetRangeUser(0,10);
	//gPad->SetLogx();
	//gPad->SetLogy();
	mg->SetTitle("Cinturon de confianza;t;tau;");
	mg->Draw("ALP");

}
