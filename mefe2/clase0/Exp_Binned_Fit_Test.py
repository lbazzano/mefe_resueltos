import numpy as np
import matplotlib.pyplot as plt
import os,sys,time
import pickle
import ROOT
import array
from ROOT import gStyle, TRandom3, TH1D, TF1, kRed, kBlue, kGreen
from decimal import *

# Set style defaults
gStyle.SetOptStat(11);       # only show histogram name and entries
gStyle.SetOptFit(1111);      # display fit results                  
gStyle.SetPadTickX(1);       # right ticks also                    
gStyle.SetPadTickY(1);       # upper ticks also                    
gStyle.SetFuncWidth(3);      # thicker function lines              
gStyle.SetHistLineWidth(3);  # define line width                    

n=40
tau=2.0

x=array.array('d',np.zeros(n))
r=TRandom3(0) 
for i in range(0,n):
    x[i]=r.Exp(tau)
                   
h1 = TH1D("h1","Generated Exponential data",20,0,10*tau)
for i in x:
    h1.Fill(i)
h1.Draw()

Expo = TF1("Expo","[a]*exp(-x/[tau])");
Expo.FixParameter(0,n/tau);  
Expo.SetParameter(1,1);      

Expo.SetLineColor(kRed);
fitResult1 = h1.Fit(Expo,"SQ");

Expo.SetLineColor(kBlue);
fitResult2 = h1.Fit(Expo,"+ SQ P");

Expo.SetLineColor(kGreen);
fitResult3 = h1.Fit(Expo,"+ SQ L");

print("(1) tau_hat: {0:.4} +/- {1:.3}".format(Decimal(fitResult1.Parameter(1)),Decimal(fitResult1.ParError(1))))
print("(2) tau_hat: {0:.4} +/- {1:.3}".format(Decimal(fitResult2.Parameter(1)),Decimal(fitResult2.ParError(1))))
print("(3) tau_hat: {0:.4} +/- {1:.3}".format(Decimal(fitResult3.Parameter(1)),Decimal(fitResult3.ParError(1))))

sys.exit(0) 
