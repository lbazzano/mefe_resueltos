auto htau1 = new TH1D("htau1","tau_hat: Option 1",200,0,5);
auto htau2 = new TH1D("htau2","tau_hat: Option 2",200,0,5);
auto htau3 = new TH1D("htau3","tau_hat: Option 3",200,0,5);

void Exp_Binned_Fit_Test (int n=400, double tau=2){

vector<double> x(n);
TRandom3 r(20);    // En cero para random numbers
for (int i = 0; i < n; ++i) x[i] = r.Exp(tau);

auto h1 = new TH1D("h1","Generated Exponential data",20,0,10*tau);
for (auto x_i : x)  h1->Fill(x_i);
h1->Draw();

auto Expo = new TF1("Expo","[a]*exp(-x/[tau])");

Expo->SetLineColor(kRed);
Expo->SetParameter(0,n/tau);  // Fix [a] at theoretical value
Expo->SetParameter(1,1);      // Initial value for free parameter [tau]
auto fitResult1 = h1->Fit(Expo,"SQ");

Expo->SetLineColor(kBlue);
Expo->SetParameter(0,n/tau);  // Fix [a] at theoretical value
Expo->SetParameter(1,1);      // Initial value for free parameter [tau]
auto fitResult2 = h1->Fit(Expo,"+ SQ P");

Expo->SetLineColor(kGreen);
Expo->SetParameter(0,n/tau);  // Fix [a] at theoretical value
Expo->SetParameter(1,1);      // Initial value for free parameter [tau]
auto fitResult3 = h1->Fit(Expo,"+ SQ L");

cout<<setprecision(3)<<fixed<<endl;   
cout<<"(1) tau_hat: "<< fitResult1->Parameter(1) << " +/- " << fitResult1->ParError(1) << endl;
cout<<"(2) tau_hat: "<< fitResult2->Parameter(1) << " +/- " << fitResult2->ParError(1) << endl;
cout<<"(3) tau_hat: "<< fitResult3->Parameter(1) << " +/- " << fitResult3->ParError(1) << endl;

}
