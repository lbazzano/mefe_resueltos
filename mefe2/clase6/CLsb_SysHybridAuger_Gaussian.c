void CLsb_SysHybridAuger_Gaussian(	int	nObs=0,
					double 	sMax=2.5,
				       	double 	bObs=0.5,
				       	double 	tau=10.0,
				       	double 	nObsEff=100,
				       	int 	nTotalEff=10,
				       	double 	alpha=0.05) {
   TRandom *rnd=new TRandom3();
   
   //double eObs=double(nObsEff)/nTotalEff; // Esperanza de la Binomial
   double eObs = 1.0;

   //double eErr=sqrt(1/nObsEff-1/nTotalEff); // Relative error (sigma/mean) for Binomial given nObsEff and nTotalEff
   double eErr = 0.0;//1; // Relative error (sigma/mean) for Binomial given nObsEff and nTotalEff
   
   //double bErr=sqrt(bObs/tau)/(bObs/tau); // Relative error (sigma/mean) for Poisson given bObs and tau
   double bErr = 0.5;
   
   int nToys = 30;//000;  // number of toys
   int nscan_points= 100;

   TGraph * g1 = new TGraph(nscan_points);
   //TF1 *f1 = new TF1("f1","TMath::Gaus(x,[0],[1]) ",0,50);
   //TF1 *f1 = new TF1("f1","ROOT::Math::gaussian_pdf(x,[1],[0]) ",0,50);
   //TF1 *f1 = new TF1("f1"," exp(-pow(x-[0],2)/(2*pow([1],2)))  /  ([1]*sqrt(2*3.142))",0,50);
   for (int i = 0; i < nscan_points; ++i) {

      double signal=-1+sMax/(nscan_points-1) * i;
      double nexp_sb = 0;  // counter for N<=NObs S+B events 
      for(int iToy=0; iToy<nToys; iToy++) {

	 double efi=0;
         /// get Efficiency from truncated Gaussian
         //while (efi<=0) efi=rnd->Gaus(eObs,eErr*eObs); // Sigma for Binomial is above
   	 TF1 *f1 = new TF1("f1","ROOT::Math::gaussian_pdf(x,[1],[0]) ",0,50);
	 f1->SetParameter(0,eObs);
	 f1->SetParameter(1,eErr*eObs);
         while (efi<=0) efi=f1->GetRandom(); 

         double b=0;
         // get Background from truncated Gaussian 
         while (b<=0) b=rnd->Gaus(bObs,bObs*bErr); //Sigma for Poisson is sqrt(mean)

         // Generate Nr of events for S+B experiments 
         int n_sb = rnd->Poisson(efi*(signal+b));

         // Count N<=nObs events for S+B experiments 
         if(n_sb<=nObs) nexp_sb += 1;

	 f1->ReleaseParameter(0);
	 f1->ReleaseParameter(1);
      }

   double CLsb = nexp_sb / nToys;
   g1->SetPoint(i,CLsb,signal);

   }

   cout <<"for Nobs="<<nObs<<", bkg="<<bObs<<"+/-"<<bErr*100<<"%, eficiencia="<<eObs<<"+/-"<<eErr*100
   << "%, \n\nSignal UL = "<< g1->Eval(alpha) << endl <<endl;

    // Create TCanvas & TFrame, and a TLine at alpha.
  
   TCanvas * canvas = new TCanvas("canvas","canvas",700,500);
   gPad->DrawFrame(0.001,-1,1,sMax-1,"UL with systematics: hybrid;1-CL;Signal UL");
   gPad->SetGridy();
   gPad->SetLogx();
   std::cout.precision(3);
   TLine *line = new TLine(alpha,-1,alpha,sMax-1);
   line->SetLineColor(kRed);
   line->SetLineWidth(2);
   line->Draw();
   g1->Draw("L");
  

}
