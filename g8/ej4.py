import numpy as np
from scipy.stats import norm
from scipy.stats import t
from scipy.stats import chi2
import matplotlib.pyplot as plt
import math

N = 1000 #cantidad de experimentos (intervalos)
mu = 10 #
sigma = 2 #
n = 3 # muestras
CL = 0.95

# quiero estimar mu y sigma es conocido
f = 0 # fracción de experimentos/intervalos que le pegaron al mu real
for exp in range(0,N):
    x = np.random.normal(mu, sigma, n)
    m = np.mean(x)
    a = norm.ppf((1-CL)/2)
    b = norm.ppf((1+CL)/2)
    L = m+a*sigma/math.pow(n,0.5)
    U = m+b*sigma/math.pow(n,0.5)
    if mu < U and mu > L:
        f += 1
print(a,b)
print('fracción de intervalos que incluyen al valor real: '+str(f))
print('fracción de intervalos que espero que lo inlcuyan: '+str(CL*N))



# quiero estimar mu y sigma es DESCONOCIDO
df = n-1
f = 0 # fracción de experimentos/intervalos que le pegaron al mu real
for exp in range(0,N):
    x = np.random.normal(mu, sigma, n)
    m = np.mean(x)
    a = t.ppf((1-CL)/2,df)
    b = t.ppf((1+CL)/2,df)
    A = x - m
    B = A*A
    s = np.sum(B)/(n-1)
    U = m-a*math.pow(s/n,0.5)
    L = m-b*math.pow(s/n,0.5)
    if mu < U and mu > L:
        f += 1
print(a,b)
print('fracción de intervalos que incluyen al valor real: '+str(f))
print('fracción de intervalos que espero que lo inlcuyan: '+str(CL*N))


# quiero estimar sigma y mu es conocido
df = n # en la presntación usa n como df pero eso me da mal (~90%). da mejor con n-1
f = 0 # fracción de experimentos/intervalos que le pegaron al mu real
for exp in range(0,N):
    x = np.random.normal(mu, sigma, n)
    m = np.mean(x)
    a = chi2.ppf((1-CL)/2,df)
    b = chi2.ppf((1+CL)/2,df)
    A = x - m
    B = A*A
    s = np.sum(B)/n
    L = n*s/b
    U = n*s/a
    if sigma*sigma < U and sigma*sigma > L:
        f += 1
print(a,b)
print('fracción de intervalos que incluyen al valor real: '+str(f))
print('fracción de intervalos que espero que lo inlcuyan: '+str(CL*N))


# quiero estimar sigma y mu es desconocido
df = n-1
f = 0 # fracción de experimentos/intervalos que le pegaron al mu real
for exp in range(0,N):
    x = np.random.normal(mu, sigma, n)
    m = np.mean(x)
    a = chi2.ppf((1-CL)/2,df)
    b = chi2.ppf((1+CL)/2,df)
    A = x - m
    B = A*A
    s = np.sum(B)/n
    L = (n-1)*s/b
    U = (n-1)*s/a
    if sigma*sigma < U and sigma*sigma > L:
        f += 1
print(a,b)
print('fracción de intervalos que incluyen al valor real: '+str(f))
print('fracción de intervalos que espero que lo inlcuyan: '+str(CL*N))
