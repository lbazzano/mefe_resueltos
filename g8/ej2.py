import numpy as np
from scipy.stats import norm
from scipy.stats import t
from scipy.stats import chi2
import matplotlib.pyplot as plt
import math

# quiero estimar mu y sigma es DESCONOCIDO
N = 1 #cantidad de experimentos (intervalos)
#mu = 10 #desconocido
#sigma = 2 # DESCONOCIDO
n = 18 # muestras
CL = 0.95
df = n-1

f = 0 # fracción de experimentos/intervalos que le pegaron al mu real
for exp in range(0,N):
    x = [128,281,291,238,155,148,154,232,316,96,146,151,100,213,208,157,48,217]
    m = np.mean(x)
    a = t.ppf((1-CL)/2,df)
    b = t.ppf((1+CL)/2,df)
    c = 2.11
    #print(a,b,c)
    A = x - m
    B = A*A
    s = np.sum(B)/(n-1)
    #U = m+c*math.pow(s/n,0.5)
    #L = m-c*math.pow(s/n,0.5)
    #print(L,U)
    U = m-a*math.pow(s/n,0.5)
    L = m-b*math.pow(s/n,0.5)
    print(L,U)
