from scipy.stats import binom
from scipy.stats import beta
import numpy as np
n = 10
k_obs = 9
print("frecuentista")
for p_L in np.linspace(0,1,10000):
    integral = binom.cdf(k_obs-1,n,p_L)
    if integral < 0.9:
        print(integral,p_L)
        break

alf = k_obs+1
bet = n-k_obs+1
p_est = float(alf)/float((alf+bet))
print("bayesiano")
'''
#print(alf,bet,p_est)
for p_min in np.linspace(0,1,10000):
    integral = beta.cdf(p_min,alf,bet)
    if integral > 0.1:
        print(integral,p_min)
        break
'''
p_min = beta.ppf(0.1,alf,bet) # mas facil usar esa función
integral  = beta.cdf(p_min,alf,bet)
print(integral,p_min)

