from scipy.stats import poisson
import numpy as np

k_obs = 6

for mu_U in np.linspace(0,15,10000):
    integral = poisson.cdf(k_obs,mu_U)
    if integral < 0.05:
        print(integral,mu_U)
        break

k_obs = 5
for mu_L in np.linspace(15,0,10000):
    integral = 1-poisson.cdf(k_obs,mu_L)
    if integral < 0.05:
        print(integral,mu_L)
        break
