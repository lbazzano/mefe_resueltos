import numpy as np
from scipy.stats import norm
from scipy.stats import t
from scipy.stats import chi2
import matplotlib.pyplot as plt
import math

# quiero estimar sigma y mu es desconocido
N = 1 #cantidad de experimentos (intervalos)
#mu = 10 #desconocido
#sigma = 2 # DESCONOCIDO
n = 10 # muestras
CL = 0.95
df = n-1

f = 0 # fracción de experimentos/intervalos que le pegaron al sigma real
for exp in range(0,N):
    x = [1002,1000,997,1001,1001,999,998,999,1000,1003]
    m = np.mean(x)
    c = chi2.ppf(1-CL,df)
    A = x - m
    B = A*A
    s = np.sum(B)/(n-1)
    U = (n-1)*s/c
    print(c,U)
